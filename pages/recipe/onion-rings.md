categories: recipe

# onion rings

- 1/2 cup all-purpose flour
- 1/4 cup cornstarch
- 2 tablespoons instant mashed potatoes
- big pinch of cayenne
- 1 cup cold club soda
- 2-3 cups Panko (Japanese-style breadcrumbs), or as needed
- fine salt to taste
- vegetable oil for frying
- 2-3 yellow onions, cut into 1/4-inch rings

## notes

the amounts in the video were for a smaller batch. The amounts above will make a nice large batch, enough for about 8 servings
