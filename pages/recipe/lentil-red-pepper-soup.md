categories: recipe

# lentil and red pepper soup

<img src="/attachments/lentil-red-pepper-soup.jpg" width="300"/>

- 2 red bell peppers, seeded and diced
- 1 onion, chopped
- 2 tablespoons (30 ml) olive oil
- 1/2 teaspoon (2.5 ml) ground turmeric
- 1/4 teaspoon (1 ml) ground cumin seeds
- 1/4 teaspoon (1 ml) ground coriander seeds
- 5 cups (1.25 litres) beef or chicken broth
- 1 cup (250 ml) red lentils, rinsed and drained
- 1 tablespoon (15 ml) tomato paste
- 2 tablespoons (30 ml) chopped cilantro
- 2 green onions, chopped
- Salt and pepper

## instructions
1. In a large saucepan, soften the bell peppers and onion in the oil. Season with salt and pepper. Add the spices and cook for 1 to 2 minutes. Add the broth, lentils, and tomato paste. Season with salt and pepper.
2. Bring to a boil. Cover and simmer gently for about 20 minutes or until the lentils are cooked. Add broth, if needed. Add the cilantro and green onion. Adjust the seasoning. Serve with whole-grain bread.
