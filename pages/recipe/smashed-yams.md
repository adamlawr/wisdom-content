categories: recipe

# smashed yams
mmm, good

<img src="/attachments/smashed-yams.jpg" width="300"/>

- 3 large Yams
- 3 tbsp (45 ml) butter
- 4 large garlic cloves, finely minced
- 2 tbsp chopped parsley
- 1/2 tsp sea salt
- pepper to taste
- 1 cup parmesan cheese

## instructions
1. Pre-heat your oven to 400 degrees F and lightly grease 2 large baking sheets.
2. Start by cutting the ends off of your yams and then slicing them into large rounds that are about 1 inch thick. Place the rounds on your baking sheets, spray lightly with cooking spray and bake for 25 minutes or until the potatoes are fork tender. If rounds are a bit thicker, bake for a few more minutes. Flip over half way during baking time.
3. Let cool a few minutes and then smash the rounds with a fork. The skin might split a bit but don’t worry about it. Do your best to keep them together.
4. In a small bowl, cream together the butter, garlic, parsley, salt and pepper. Spread the butter mixture evenly over the potato rounds.
5. Bake for 5 additional minutes then switch on the broiler and broil until the edges start to get crispy.
6. Remove from the oven and sprinkle the Parmesan cheese over the rounds and return to the oven until the cheese is melted. It should take about 3 minutes.
7. Once the cheese is melted to your liking, remove from the oven. You can sprinkle with extra parsley if you desire. Then serve immediately.

## notes
- from [save on foods](https://www.saveonfoods.com/sm/pickup/rsid/987/recipes/167efbf0-5bd2-40cc-9cbe-ac2c595d96a8)
- haven't tried these yet
