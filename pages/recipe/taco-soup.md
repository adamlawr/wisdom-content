categories: recipe

# taco soup

- 1 tbsp (15 mL) olive oil
- 1-1/2 lb (680 g) lean ground beef
- 1 cup (250 mL) chopped onion
- 1 clove garlic, minced
- 1 can (28 oz/796 mL) diced tomatoes
- 1 can (19 oz/540 mL) black beans, drained and rinsed
- 1 can (14 oz/398 mL) tomato sauce
- 1 can (12 oz/341 ml) corn, with juice
- 3/4 cup (175 mL) water
- 2 tbsp (30 mL) chili powder
- 1-1/2 tsp (7 mL) salt
- 1 tsp (5 mL) ground cumin
- 1/2 tsp (2 mL) cayenne pepper or red pepper flakes
## toppings
- sour cream
- shredded cheddar cheese
- crumbled corn taco shell

## instructions

1. cook the rice
2. brown the ground beef well. Add onion and garlic; cook for 5 minutes.
3. stir in tomatoes, beans, tomato sauce, corn, water, chili powder, salt, ground cumin and
cayenne pepper
4. bring to boil. Reduce heat to low; cook for about 30 minutes.
5. Taste and adjust seasonings as desired.
6. Spoon 2 cups (500 mL) soup into each soup bowl.

## notes
- serve with rice
- top with sour cream, cheese and/or crumbled taco shell
