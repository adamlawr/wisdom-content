categories: recipe

# mozzarella sticks:

- 2 eggs, beaten
- 1/4 cup water
- 1 1/2 cups Italian seasoned bread crumbs
- 1/2 teaspoon garlic salt
- 2/3 cup all-purpose flour
- 1/3 cup cornstarch
- 1 quart oil for deep frying
- 1 (16 ounce) package mozzarella cheese sticks

## instructions

1.	In a small bowl, mix the eggs and water.
2.	Mix the bread crumbs and garlic salt in a medium bowl. In a medium bowl, blend the flour and cornstarch.
3.	In a large heavy saucepan, heat the oil to 365 degrees F (185 degrees C).
4.	One at a time, coat each mozzarella stick in the flour mixture, then the egg mixture, then in the bread crumbs and finally into the oil.
5. Fry until golden brown, about 30 seconds.
6. Remove from heat and drain on paper towels.
