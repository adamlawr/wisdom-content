categories: recipe

# meatballs (veggie)    

<img src="/attachments/meatballs.jpg" width="300"/>

- 1 tablespoon olive oil
- 1 pound mushrooms, finely chopped
- 1 pinch salt
- 1 tablespoon butter
- 1/2 cup finely chopped onion
- 4 cloves garlic, minced
- 1/2 cup quick-cooking oats
- 1/2 cup parmesan cheese
- 1/2 cup bread crumbs
- 1/4 cup chopped parsley or spinnach
- 2 eggs
- 1 teaspoon salt
- ground black pepper to taste
- 1 pinch cayenne pepper
- 1 pinch dried oregano

## instuctions

1. cook mushrooms in oil in a hot skillet liquid from mushrooms has evaporated. sprinkle with salt, reduce heat to medium, and cook until golden brown, about 5 minutes.
2. add butter and onions and cook, until onion is translucent, 5 minutes. stir in garlic for the last minute
3. transfer mixture to a mixing bowl
4. mix in oats, bread crumbs, parmesan, 1 egg, parsley or spinnach until thoroughly combined. 
5. season with salt, black pepper, cayenne pepper, and oregano
6. stir in the other egg. mixture should hold together when pressed
7. cover with plastic wrap and refrigerate at least 2 hours
8. preheat oven to 450
9. form mixture into small meatballs
10. bake in preheated oven on parchment paper until lightly golden brown, 12 to 15 minutes

11. optional - simmer meatballs in pasta sauce until cooked through, 45 minutes to 1 hour

## notes
- [from](https://www.allrecipes.com/recipe/232908/chef-johns-meatless-meatballs/)
- can be frozen and re-heated
