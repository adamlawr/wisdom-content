categories: recipe

# chocolate chip cookies
mmm, good

<img src="/attachments/chocolate-chip-cookies.jpg" width="300"/>

- 1 cup butter
- 1 cup white sugar
- 1 cup brown sugar
- 2 eggs
- 2 tsp vanilla extract
- 1 tsp baking soda disolved in 2 tsp hot water
- ½ tsp salt
- 3 cups flour
- 2 cups chocolate chips
- 1 cup chopped walnuts (or brazil nuts)

## instructions 
1. Preheat oven to 350
2. Cream together the butter, white sugar, and brown sugar until smooth. 
3. Beat in the eggs, then stir in the vanilla. Dissolve baking soda in hot water. Add to batter along with salt. 
4. Stir in flour, chocolate chips, and nuts.
5. Drop by large spoonfuls onto ungreased pans.
6. Bake for about 10 minutes in the preheated oven.

## notes
- [from](https://www.allrecipes.com/recipe/10813/best-chocolate-chip-cookies)
