categories: recipe

# scones

- testing...
- 3 cups flour
- 1/3 cup sugar
- 5 teaspoons baking powder
- 1 teaspoon salt
- 1/2 pound (2 sticks) butter, chilled
- 1/4 cup pecans
- 1 large egg
- 3/4 cup heavy cream

## icing
- 1 pound powdered sugar
- 1/4 cup whole milk
- 4 tablespoons (1/2 stick) butter, melted
- splash of strongly brewed coffee
- dash of salt
- 2 teaspoons maple flavouring or maple extract

## instructions
1. Preheat oven to 350 degrees.
2. In a large bowl stir together the flour, sugar, baking powder and salt.
3. Cut the cold butter into small pieces. Cut the butter into the flour until the mixture resembles crumbs.
4. finely chop the pecans, stir them into the flour mixture.
5. Mix the egg and cream together, add the mixture to the bowl. Stir together until just combined.
6. Turn the mixture onto a cutting board, push the mixture together into a large ball. Do not knead or press too much.
7. roll the dough into a 10-inch round, about 3/4 inch thick.
8. cut the round into eight equal sized wedges (like a pizza).
9. Transfer the wedges to a baking sheet lined with parchment paper.
10. Bake for 22-26 minutes, until just starting to brown. Allow to cool completely before icing.

11. mix icing ingredients until smooth.
12. Pour generously over the scones. Allow the icing to set before serving.

## notes
- Optional: Sprinkle chopped pecans over the icing before it sets.
