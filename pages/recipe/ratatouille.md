categories: recipe

# ratatouille provencaie

- extra virgin olive oil
- 2 medium red onions, diced
- 10 to 12 garlic cloves, thinly sliced
- 1 bottle white wine
- 1/4 cup tomato paste
- 2 cans diced tomatoes with juice (or 6-8 ripe tomatoes)
- sea salt, pepper
- 2 large eggplants diced
- 2 large zucchinni diced
- 6 red or yellow peppers
- 1/4 bunch fresh oregano chopped
- 1 bunch fresh basil chopped

## instructions

1. preheat oven 400F

2. brown onions (6 minutes)
3. add garlic (2-3 minutes)
4. add white wine, tomatoe paste, and tomatoes
5. season with salt and pepper
6. simmer 30 minutes until slightly thickened

While the sauce simmers, combine eggplant, peppers and zucchinni in a large bowl
toss with olive oil and season with salt and pepper
roast about 15 minutes on 1 or two baking trays until slightly dried out

### topping

- olive oil
- 2 cups bread crumbs or shredded bread
- 2 cloved of garlic minced
- 1/2 bunch fresh parsley chopped
- 1/2 log goats cheese crumbled

1. toss bread crumbs in olive oil
2. mix in garlic and parsley

### finish

1. when the sauce has thickened, stir in basil and oregano
2. mix in roasted vegetables
3. season to taste with salt and pepper
4. transfer to casserole dish
5. top with bread crumbs and crumbled goat cheese

6. bake ~10 minutes until golden brown
