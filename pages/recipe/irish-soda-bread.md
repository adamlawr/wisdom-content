categories: recipe

# irish Soda Bread

- 4c flour
- ½c sugar (less)
- 1t salt
- 1t baking soda
- ½c margarine
- 1 ½c raisins
- 1 1/3 buttermilk
- 1 egg
- 1t baking soda

## instructions

1. preheat oven to 375
2. mix flour, sugar, salt and powder
3. cut in margarine
4. stir in raisins
5. combine buttermilk, egg and soda
6. stir buttermilk mixture into dry until moistened
7. bake in buttered 2 qt casserolle for 45-50 minutes
