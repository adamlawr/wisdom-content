categories: recipe

# cauliflower gratin

<img src="/attachments/cauliflower-gratin.jpg" width="300"/>

- 1 (3-pound) head cauliflower, cut into large florets
- Kosher salt
- 4 tablespoons (1/2 stick) unsalted butter, divided
- 3 tablespoons all-purpose flour
- 2 cups hot milk
- 1/2 teaspoon freshly ground black pepper
- 1/4 teaspoon grated nutmeg
- 3/4 cup freshly grated Gruyere, divided
- 1/2 cup freshly grated Parmesan
- 1/4 cup fresh bread crumbs

## instructions

1. Preheat the oven to 375 degrees F.
2. Cook the cauliflower florets in a large pot of boiling salted water for 5 to 6 minutes, until tender but still firm. Drain.
3. Meanwhile, melt 2 tablespoons of the butter in a medium saucepan over low heat. Add the flour, stirring constantly with a wooden spoon for 2 minutes. Pour the hot milk into the butter-flour mixture and stir until it comes to a boil. Boil, whisking constantly, for 1 minute, or until thickened. Off the heat, add 1 teaspoon of salt, the pepper, nutmeg, 1/2 cup of the Gruyere, and the Parmesan.
4. Pour 1/3 of the sauce on the bottom of an 8 by 11 by 2-inch baking dish. 
5. Place the drained cauliflower on top and then spread the rest of the sauce evenly on top. 
6. Combine the bread crumbs with the remaining 1/4 cup of Gruyere and sprinkle on top. Melt the remaining 2 tablespoons of butter and drizzle over the gratin. Sprinkle with salt and pepper. 
7. Bake for 25 to 30 minutes, until the top is browned. Serve hot or at room temperature.

## notes
- [from](https://www.foodnetwork.com/recipes/ina-garten/cauliflower-gratin-recipe-1917463)
