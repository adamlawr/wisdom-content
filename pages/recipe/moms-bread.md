categories: recipe

# mom's bread

- 4c Water
- 2/3 c Sugar or Honey  ~ Heat to melt butter – cool to lukewarm
- 3T Salt
- ½c Margarine

        4T Yeast
Proof ~ 2c Water (very warm)
        Sprinkle of Sugar

## instructions

1. Add yeast to lukewarm mix in large bowl until it has doubled
2. add and mix: sunflower and sesame seeds, wheat germ, oat and wheat bran bulgur
3. add flour (whole wheat and some white) 6-8 cups until ball is soft but not sticky – should feel nice
4. put in greased bowl ~ cover and let rise 30-60 minutes punch down ~
5. knead and shape
6. put in greased pans, turn buttered side up
7. preheat oven to 400° ~ turn down to 375° add water carefully.  Done when sounds hollow when dropped.

## notes
- makes 6 loaves
