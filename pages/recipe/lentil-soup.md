categories: recipe

# lentil soup

<img src="/attachments/lentil-soup.jpg" width="300"/>

## ingredients
- 2 medium carrots 
- 1 medium yellow onion 
- 4 cloves garlic
- 2 cups dried red lentils (about 1 pound) 
- 3 tablespoons olive oil 
- 1 tablespoon curry powder 
- 1 teaspoon ground cumin 
- 1 teaspoon dried thyme 
- 1/2 teaspoon kosher salt
- 1 (15-ounce) can tomato sauce 
- 6 cups low-sodium chicken or vegetable broth 
- 5 cups baby spinach (about 5 ounces)  

## instructions

    Peel and dice 2 medium carrots and 1 medium yellow onion. Mince 4 cloves garlic.

    Place 2 cups dried red lentils (about 1 pound) in a fine-mesh strainer. Pick through the lentils, discarding any broken or discolored ones. Rinse under cool water until the water runs clear, about 1 minute. Set aside to drain while you cook the vegetables.

    Heat 3 tablespoons olive oil in a large pot or Dutch oven over medium-high heat until shimmering. Add the carrots and onion and sauté until softened, about 5 minutes. Add the garlic, 1 tablespoon curry powder, 1 teaspoon ground cumin, 1 teaspoon dried thyme, and 1/2 teaspoon kosher salt. Cook until fragrant, about 1 minute.

    Add the lentils, 1 (15-ounce) can tomato sauce, and 6 cups broth, stir to combine, and bring to a simmer. Cover and reduce the heat to maintain a gentle simmer. Cook, stirring occasionally until the lentils are tender, 18 to 22 minutes.

    Remove from the heat and stir in 5 cups spinach until wilted. Serve immediately. 

## notes
- [from](https://www.thekitchn.com/lentil-soup-recipe-23004361)
