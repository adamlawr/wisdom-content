categories: recipe

# squash chickpea casserole

6-8 servings

<img src="/attachments/squash-chickpea-casserole.jpg" width="300"/>

- 1 butternut squash
- 1 pound of mushrooms
- 2 large onions
- 3 large peppers
- 3 cans of chickpeas
- 2 cans of chopped tomatoes
- at least 2 tablespoons of olive tapanade
- at least 2 tablespoons of red wine vinegar
- pepper
- olive oil

## instructions

1. Preheat oven to 350 degrees Fahrenheit.
2. cut the squash legthwise and empty. sprinkle with olive oil and salt. bake
3. cut or tear up the mushrooms into a hot dutch oven on the burner with no oil. stir and let the moisture come out of the mushrooms for 10 minutes.
4. cut the peppers into large chunks and add to the mushrooms to char on high heat.
5. cut the onions into large chunks and add to the mushrooms to char on high heat.
6. add salt and pepper and stir for another 10 minutes.
7. add chopped garlic, 2 tablespoons of olive oil, olive tapanade and red wine vinegar.
8. add tomatoes, some water with the tomatoe cans, and chickpeas with the liquid
9. boil and add seasoning.
10. place the squash from the oven on top, skin side down, and return the whole lot to the oven for 60-80 minutes until the squash is soft
11. season with more salt, pepper and red wine vinegar if neccessary.


## notes

- from [here](https://www.jamieoliver.com/recipes/vegetable-recipes/chickpea-squash-casserole/)
- or [video](https://www.facebook.com/watch/?v=804269220778870)
- serve on its own or with toast, pesto and mozzerella
