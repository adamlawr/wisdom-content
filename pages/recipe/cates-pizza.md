categories: recipe

# pizza crust

- 1 1/2 cups warm water
- 1 teaspoon dry yeast
- 4 cups flour
- 1 teaspoon sea salt
- 1/3 cup olive oil

## instructions

1. sprinkle yeast over warm water.
2. In a large bowl combine the flour and salt.
3. drizzle and mix the olive oil into the flour.
4. stir the water and yeast
5. mix it into the flour and form a ball with your hands
6. coat a clean bowl with some olive oil
7. coat the dough in the oil bowl
8. cover with a damp towel for 1-2 hours
9. Preheat oven to 500 degrees.
10. divide dough in half
11. coat baking sheet with oil and stretch the dough to fit
10. Bake for 8-10 minutes

## notes
- from cate's cook book
