categories: recipe

# pie crust
you know, for willmotts

<img src="/attachments/pie-crust.jpg" width="300"/>

- 2 1/2 cups all-purpose flour
- 1/2 Tbsp granulated sugar
- 1/2 tsp sea salt
- 1/2 lb COLD unsalted butter diced into 1/4" pieces
- 6 Tbsp ice water (6 to 7 Tbsp)

## instructions
1. Place flour, sugar and salt into the bowl of a food processor and pulse a few times to combine.
2. Add cold diced butter and pulse the mixture until coarse crumbs form with some pea-sized pieces then stop mixing. Mixture should remain dry and powdery.
3. Add 6 Tbsp ice water and pulse just until moist clumps or small balls form. Press a piece of dough between your finger tips and if the dough sticks together, you have added enough water. If not, add more water a teaspoon full at a time. Be careful not to add too much water or the dough will be sticky and difficult to roll out.
4. Transfer dough to a clean work surface, and gather dough together into a ball (it should not be smooth and DO NOT knead the dough). Divide dough in half and flatten to form 2 disks. Cover with plastic wrap and refrigerate 1 hour before using in recipes that call for pie crust.

## notes
- from [here](https://natashaskitchen.com/easy-pie-crust-recipe/)
