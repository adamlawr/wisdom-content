categories: recipe

# stuffed butternut squash

<img src="/attachments/stuffed-butternut-squash.jpg" width="300"/>

- 2 medium butternut squash (2 to 2 1/4 pounds each)
- 3 tablespoons apple cider vinegar
- 2 tablespoons pure maple syrup
- 4 tablespoons olive oil
- Kosher salt and freshly ground black pepper
- 1 small onion, chopped
- 1/2 cup wild rice
- 1/2 teaspoon mild curry powder
- 1/2 teaspoon ground cinnamon
- 1/4 teaspoon cayenne pepper
- 3 tablespoons dried unsweetened cherries
- 1 cup loosely packed fresh parsley leaves, chopped, plus more for garnish
- 1 tablespoon chopped fresh sage
- 1/4 cup walnuts, coarsely chopped

## instructions

### squash
1. Position an oven rack in the middle of the oven and preheat to 400 degrees F.
2. Cut each squash in half lengthwise and scoop out and discard the seeds. Arrange the halves in a large baking dish, flesh-side up.
3. Whisk together the vinegar, maple syrup and 2 tablespoons oil. 
4. Brush the flesh-side of the squash halves with some of the maple-oil and sprinkle with 1/4 teaspoon salt and a few grinds of pepper. 
5. Put the squash flesh-side down in the baking dish, then brush the skin side with the maple-oil mixture and sprinkle with 1/4 teaspoon salt and a few grinds of pepper. 
6. Roast until the squash is fork-tender, 30 to 40 minutes. 
7. Let the squash rest until cool enough to handle. 
8. Scoop some of the flesh out into a large bowl, leaving about 1/4-inch border of flesh all around. Leave the scooped-out flesh in relatively large chunks.

### stuffing
1. heat 1 tablespoon oil in a medium saucepan over medium-high heat, then add the onions and cook, stirring occasionally, until soft and golden brown, about 6 minutes. 
2. Add the rice, curry powder, cinnamon, cayenne and 1/2 teaspoon salt and stir until the spices are toasted, about 1 minute. 
3. Add 2 cups water and bring to a simmer covered, stirring occasionally, until the rice is tender and most of the liquid is absorbed, 30 to 40 minutes (different brands of wild rice may vary in cooking times; add more water if needed). 
4. Remove from heat and add to the chunks of butternut squash along with the remaining maple-oil, cherries, parsley, sage, 1/2 teaspoon salt and a few grinds of black pepper.

### combine
1. Evenly stuff the scooped-out squash halves with the filling, then drizzle with the remaining 1 tablespoon oil and bake, uncovered, until the filling is warmed through, about 30 minutes. 
2. Cut each in half crosswise and transfer to a serving platter. Sprinkle with walnuts and parsley. Serve warm.

## notes

- [from](https://www.foodnetwork.com/recipes/food-network-kitchen/vegan-wild-rice-stuffed-butternut-squash-3362734)
