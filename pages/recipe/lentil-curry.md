categories: recipe

# lentil curry

<img src="/attachments/lentil-curry.jpg" width="300"/>

- 3 tablespoons coconut oil
- 1 1/2 cups finely diced yellow onion
- 1 cup finely diced carrot
- 4 cloves finely minced garlic
- 1 1/2 tablespoon finely minced ginger
- 2 tablespoons red curry paste
- 1 tablespoon yellow curry powder
- 1 1/2 teaspoon EACH: garam masala, paprika, ground coriander
- 1 teaspoon ground cumin
- 1 teaspoon turmeric
- 1 1/2 cup crushed tomatoes
- 1 teaspoon white sugar
- 1 cup red or green lentils
- 1 1/2 cups coconut milk
- 3 cups vegetable stock
- 1/2 cup finely diced cilantro
- Optional: zest and juice of a lemon

## instructions
1. rinse well and soak lentils for 20 minutes
2. finely dice onion, garlic, ginger, carrots, celery
3. saute onions, garlic and ginger in hot coconut oil, 2-3 minutes
4. add carrots and celery, 6-8 minutes
5. add curry paste and spices, increase heat for 1-2 minutes
6. add crushed tomatoes, lower heat, 1-2 minutes
7. add lentils, stock and coconut milk
8. simmer 30 minutes, 10 minutes uncovered
9. add cilantro and optional lemon juice
10. serve with rice

## notes
1. [from](https://www.yummly.com/recipe/Lentil-Curry-9407755)