categories: recipe

# bread sticks
mmm, good

<img src="/attachments/bread-sticks.jpg" width="300"/>

- 1 1/2 cup Warm Water
- 1 Tablespoon Sugar
- 1 teaspoon Salt
- 1 Tablespoon Dry Active Yeast
- 3 1/2 cups Flour
- ½ cups Melted Butter
- 1 cup Grated Parmesan Cheese
- Lemon Pepper Or Greek Seasoning

## instructions
1. Mix together warm water, sugar, salt and yeast. Allow yeast to activate (about 10 minutes).
2. Add flour and stir in with a fork until dough holds together. Turn out onto floured board.
3. Knead lightly into a ball. Divide into 32 equal portions.
4. On floured board, roll out into bread sticks.
5. Dip into melted butter and then into Parmesan cheese. Place onto greased cookie sheet (16 per bakers half sheet.)
6. Sprinkle with lemon pepper.
7. Preheat oven to 400 degrees. This gives the bread sticks about 10 minutes to begin to rise.
8. Cook for 15 minutes.

## notes
- [from](https://cookieandkate.com/2016/20-simple-vegetarian-dinners/)
- may be made in the morning and kept in the refrigerator unbaked
- remove from the refrigerator half an hour before baking
