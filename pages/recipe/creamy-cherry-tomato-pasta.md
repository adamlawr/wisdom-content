categories: recipe

# creamy cherry tomato & summer squash pasta
Simple summer pasta recipe featuring roasted cherry tomatoes, zucchini and yellow squash with pasta and a light goat cheese sauce. Recipe yields 4 servings.

<img src="/attachments/creamy-cherry-tomato-pasta.jpg" width="300"/>

- 2 cups rotini, fusilli, penne or shells pasta
- 1 pint cherry tomatoes
- 2 medium yellow squash, quartered vertically and then sliced into wedges
- 1 medium zucchini, quartered vertically and then sliced into wedges
- 2 tablespoons olive oil
- Salt and freshly ground black pepper, to taste
- 2 tablespoons lemon juice
- 2 tablespoons butter or olive oil
- 100g goat cheese, crumbled
- 1 clove garlic
- Pinch red pepper flakes
- small handful chopped fresh basil or cilantro

## instructions

1. Preheat oven to 400
2. Toss the whole cherry tomatoes, zucchini and squash with olive oil. Sprinkle with salt and pepper
3. Bake in a single layer on parchment paper for about 25 minutes, tossing halfway, until the cherry tomatoes have burst and the squash is tender.
4. Cook the pasta until al dente. Save 1 cup of the pasta cooking water. Drain the pasta and return it to the pot.
5. While the pasta is hot, add the lemon juice, butter, goat cheese, garlic and red pepper flakes to the pot. Add about 1/4 cup of the pasta water and gently toss until the pasta is lightly coated.
6. Add the roasted vegetables. Gently toss to combine. Season with salt and freshly ground pepper.
7. Sprinkle chopped basil over the pasta.
8. Serve immediately.

## notes

- [from](https://cookieandkate.com/2016/20-simple-vegetarian-dinners/)
