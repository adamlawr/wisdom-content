categories: recipe

# broccoli cheese casserole

6-8 servings

<img src="/attachments/broccoli-casserole.jpg" width="300"/>

- 2 cups vegetable broth or water
- 1 cup quinoa (any color), rinsed under running water in a mesh sieve for a minute and drained
- 16 ounces broccoli florets, either pre-packaged or sliced from 2 large bunches of broccoli
- 2 tablespoons olive oil
- ¾ teaspoon salt
- 10 twists of freshly ground black pepper
- ¼ teaspoon red pepper flakes, omit if sensitive to spice
- 8 ounces (about 2 ½ cups) freshly grated cheddar cheese, divided
- 1 cup low-  milk (cow’s milk tastes best but unsweetened plain almond milk works, too)
- ½ tablespoon butter or 1 ½ teaspoons olive oil
- 1 clove garlic, pressed or minced
- 1 slice whole wheat bread (substitute gluten-free bread for a gluten-free casserole)

## instructions

1. Preheat oven to 400 degrees Fahrenheit.
2. Line a large, rimmed baking sheet with parchment paper for easy cleanup.
### To cook the quinoa
3. Bring the vegetable broth or water to boil in a heavy-bottomed, medium-sized pot.
4. Add the quinoa, reduce heat to low and simmer, uncovered, for 17 to 20 minutes, or until all of the liquid is absorbed.
5. Cover and set aside to steam for 10 minutes.
### To roast the broccoli
6. Slice any large broccoli florets in half to make bite-sized pieces.
7. Transfer the broccoli to your prepared baking sheet and toss with 2 tablespoons olive oil, until lightly coated on all sides.
8. Sprinkle with salt and arrange in a single layer. Bake for about 20 minutes, until the broccoli is tender and starting to caramelize on the edges.
### To make the breadcrumbs
9. Tear your piece of bread into bite-sized pieces and toss them into a food processor or blender. Process until the bread has broken into small crumbs.
10. In a small pan over medium heat, melt the butter.
11. Add the garlic and cook just until fragrant, stirring often.
12. Add the bread crumbs and cook for 2 to 3 minutes, until slightly browned and crisp. Set aside to cool. (If you cooked your bread crumbs in a cast iron skillet, transfer them to a bowl to prevent them from burning.)
13. Reduce the oven heat to 350 degrees. Add the salt, pepper and red pepper flakes to the pot of quinoa, and stir to combine. Set aside ¾ cup of the cheese for later, then add the cheese to the pot.
14. Pour in the milk and stir until the cheese and milk are evenly incorporated in the quinoa.
Pour the cheesy quinoa into a 9-inch square baking dish and top with the roasted broccoli. Stir until the broccoli is evenly mixed in with the quinoa. Sprinkle the surface of the casserole with the reserved ¾ cup cheese, then sprinkle the breadcrumbs on top.
Bake, uncovered, for 25 minutes, until the top is golden. Allow to cool for 10 minutes before serving.

## notes

- [from](https://cookieandkate.com/2016/20-simple-vegetarian-dinners/)
- Recipe adapted from my creamy roasted Brussels sprout and quinoa gratin, which was originally adapted from The High-Protein Vegetarian Cookbook by Katie Parker of Veggie and the Beast.
- Storage suggestions: Leftovers keep well in the refrigerator, covered, for up to 4 days. Gently reheat in the oven or microwave before serving.
