categories: recipe

# oatmeal cookies
mmm, good

<img src="/attachments/oatmeal-cookies.webp" width="300"/>

- 1 cup butter, softened
- 1 cup white sugar
- 1 cup packed brown sugar
- 2 eggs
- 1 teaspoon vanilla extract
- 2 cups flour
- 1 tsp baking soda
- 1 tsp salt
- 1 1/2 tsp cinnamon
- 3 cups quick oats

## instructions
1. cream together butter, white sugar, and brown sugar. beat in eggs, and stir in vanilla
2. combine flour, baking soda, salt, and cinnamon; stir into the creamed mixture
3. mix in oats
4. optional chill for an hour
5. preheat oven to 375 degrees
6. bake for 9 to 11 minutes.

## notes
- [from](https://www.allrecipes.com/recipe/19247/soft-oatmeal-cookies/)
