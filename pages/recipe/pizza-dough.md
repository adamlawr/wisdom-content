categories: recipe

# pizza dough - John Palko's

- 1t yeast
- 2¼c warm water
- 3¼ – 4c flour (w.w.?)
- ½t salt

## instructions
1. proof yeast in warm water with a sprinkle of sugar
2. combine flour and salt in bowl
3. add yeast to dry mixture—mix and knead
4. let rise for 10 minutes—punch dough down and form into crusts on oiled pans

## notes
- makes two crusts
