categories: recipe

# sugar cookies

- 2 cups butter, at room temperature
- 2 cups sugar
- 2 tbsp vanilla
- 5 cups flour
- 2 large eggs
- 2 tsp salt

## instructions

1. Cream butter and sugar until light and fluffy.
2. mix in eggs, vanilla and salt
3. add flour in 2 batches
4. Divide dough into four equal portions.
5. Place each on a piece of plastic wrap. Flatten into disks.
6. Wrap in plastic wrap; refrigerate until firm, at least 2 hours or up to one week.

7. Roll, cut, chill until firm
8. bake at 350 for about 15 minutes.
