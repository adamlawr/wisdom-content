categories: recipe

# roasted garlic and herb no-knead bread

<img src="/attachments/garlic-herb-bread.jpg" width="300"/>

Measure 3 cups all-purpose flour
1 teaspoon kosher salt, and 3/4 teaspoon active dry yeast into a large bowl. 

Make a well in the flour mixture, add 1 1/2 cups warm water, and stir until a rough, shaggy dough forms. 

Cover the bowl and let the dough rise in a warm place until doubled in bulk and bubbly, 6 to 8 hours 

While the dough rises, roast a large head of garlic: Remove the excess papery skins from the head and slice a thin layer off the top to expose the cloves. Wrap the garlic completely in aluminum foil and bake at 400ºF until tender, about 30 minutes. Let cool completely, then squeeze the roasted cloves out of their skins and reserve. 

Lightly flour a piece of parchment paper and turn the dough out onto it. Sprinkle the dough with the roasted garlic cloves, 1 tablespoon fresh thyme leaves, and 2 teaspoons finely chopped fresh rosemary leaves. Fold the dough over onto itself 4 times to incorporate the add ins. Flip the dough over and quickly shape it into a round ball. Cover with a towel and let rise until nearly doubled in size, 1 to 1 1/2 hours.

Once the loaf is in the Dutch oven, quickly drizzle with 2 tablespoons olive oil and sprinkle with 1 teaspoon flaky salt.  Bake as directed above. 

## notes
- [from](https://www.thekitchn.com/roasted-garlic-herb-no-knead-bread-22987118)
