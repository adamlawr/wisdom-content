categories: recipe

# pineapple upsidedown cake

<img src="/attachments/pineapple-upsidedown-cake.jpg" width="300"/>

- 4 large eggs eggs
- ½ cup butter
- 1 cup packed light brown sugar
- 1 (20 ounce) can sliced pineapple
- 10 cherries maraschino cherries, halved
- 1 cup sifted cake flour
- 1 teaspoon baking powder
- ¼ teaspoon salt
- 1 cup white sugar
- 1 tablespoon butter, melted
- 1 teaspoon almond (or vanilla) extract

## instructions
1. preheat oven to 325.
2. in glass casserole pan, melt 1/2 cup butter in preheating oven. add brown sugar and mix. arrange pineapple slices to cover bottom of skillet. set aside.
3. combine flour, baking powder, and salt in a small bowl.
4. separate 4 eggs with the whites in a large bowl. beat egg whites just until soft peaks form. add white sugar gradually. beat until medium-stiff peaks form. 
5. beat egg yolks at high speed until very thick and yellow.
6. with rubber scraper, gently fold egg yolks and flour mixture into whites until blended. 
7. fold in 1 tablespoon melted butter or margarine and almond extract.
8. spread batter evenly over pineapple.
9. bake until surface springs back when gently pressed with fingertip and a toothpick inserted in the center comes out clean, about 30 to 35 minutes. Cool for 5 minutes, loosen the edges, invert onto serving plate.

## notes
- from [all recipes](https://www.allrecipes.com/recipe/7622/old-fashioned-pineapple-upside-down-cake)
