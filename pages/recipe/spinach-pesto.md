categories: recipe

# spinach pesto
get rid of that wilting spinach

<img src="/attachments/spinach-pesto.jpg" width="300"/>

- 2 cups baby spinach leaves
- 1/4 cup fresh basil leaves
- 1/4 cup fresh parsley
- 1/2 cup chopped walnuts OR pine nuts
- 1/2 cup parmesan cheese
- 3 garlic cloves
- 1 tablespoon fresh lemon juice
- 1/2 teaspoon lemon zest
- salt and fresh ground pepper to taste
- 1/3 cup extra virgin olive oil

## instructions
1. Combine all ingredients in a food processor; process until smooth.
2. Keep in the refrigerator for up to two weeks.

## notes
- [from](https://diethood.com/spinach-pesto)
