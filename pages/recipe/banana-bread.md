categories: recipe

# banana bread
mmm, good

<img src="../attachments/banana-bread.jpg" width="300"/>

![[banana-bread.jpg]]


- 3/4 cup butter
- 3/4 cup brown sugar
- 2 eggs
- 2 1/3 cups bananas
- 2 cups flour
- 1 tsp baking soda
- ½ tsp salt
- 1 cup chopped walnuts

## instructions 
1. Preheat oven to 350
2. Cream together the butter, and brown sugar. 
3. Beat in the eggs, then stir in the bananas. 
4. Stir in flour, and nuts.
5. Grease the pans and fill about half full.
6. Bake for 60-65 minutes until a knife comes out dry.

## notes
- [from](https://www.allrecipes.com/recipe/20144/banana-banana-bread/)
