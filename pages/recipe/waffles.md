categories: recipe

# waffles
mmm, good

<img src="/attachments/waffles.jpg" width="300"/>

- 2 cups flour
- 2 tsp baking powder
- 1 tsp baking soda
- 1/2 tsp salt
- 2 cups buttermilk
- 2 eggs
- 1/3 cup oil

## instructions 
1. Preheat waffle iron and make sure it's oiled
2. make the buttermilk with regular milk and a splash of white vinegar. 
3. Beat the eggs in the buttermilk. 
4. Combine the dry stuff.
5. Mixin the buttermilk mixture and the oil.

## notes
- from - who knows, been using this recipe forever
