categories: recipe

# cauliflower pasta

- 1/2 head cauliflower
- cup cheddar cheese grated
- parmesan cheese
- bunch of fresh parsley
- sea salt
- 4 cups of macaroni
- 1 cup of sour cream

## instructions

1. cut cauliflower into small florets
2. grate the cheddar and parmesan into a large heat proof bowl
3. finely chop the parsley stalks and leaves

4. preheat broiler
5. add macaroni and cauliflower to boiling water
6. place the bowl of cheese over the saucepan and add the sour cream
7. stir until the cheese is melted. if water boils up, turn down the heat.
8. add chopped parsley to the cheese and season with the sea salt and pepper
9. remove bowl and drain water into another bowl
10. return pasta to the pan and add the cheese sauce. if it's too thick, add some of the cooking water
11. put in a baking dish and broil until it's golden and bubbling
