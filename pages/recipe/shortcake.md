categories: recipe

# shortcake

- 2 1/2 cups flour
- 4 teaspoon baking powder
- 1/2 teaspoon salt
- 2 tablespoons sugar

- 1/4 cup butter
- 3/4 cup milk
- 1/4 cup water

## instructions

1. stir dry ingredients
2. cut in butter until crumbly
3. combine milk and water and heat until warm
4. add 7/8 to dry ingredients
5. toss mixture with fork
6. knead about 10 times
7. divide in two and pat down in 2 8" pans
8. bake at 450 for 10-12 minutes
