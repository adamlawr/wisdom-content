categories: recipe

# southwest dressing

<img src="/attachments/southwest-dressing.jpg" width="300"/>

- 1 cup plain greek yogurt
- 1 lime juiced
- 1/2 tsp chili powder
- 1/2 tsp paprika
- 1/2 tsp cumin
- 1/2 tsp garlic powder
- 2 tsp honey
- 1/2 tsp salt
- 1/2 tsp pepper
- 1 tsp cilantro finely chopped
- 2 tsp water as needed

## instructions 
1. mix it all together

## notes
- optional 1/4 tsp cayenne peeper for spice
- [from](https://www.eatingonadime.com/southwest-salad-dressing-recipe/)
