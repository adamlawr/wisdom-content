categories: recipe

# corn bread

<img src="/attachments/corn-bread.jpg" width="300"/>

- 1/2 cup unsalted butter melted and cooled slightly
- 1 cup all-purpose flour
- 1 cup yellow cornmeal
- 1/4 cup brown sugar
- 1 tsp baking powder
- 1/2 tsp baking soda
- 1/4 tsp salt
- 1 cup buttermilk
- 1/4 cup honey
- 2 eggs

## instructions
1. preheat the oven to 400
2. grease a 9-inch square baking dish and place in the oven to heat up
3. whisk together the flour, cornmeal, sugar, baking powder, baking soda and salt
4. add the buttermilk, honey and eggs
5. then add in the slightly cooled melted butter. mix again just until combined
6. remove the hot baking dish from the oven and pour in the batter
7. bake until browned on top and an inserted knife comes out clean (about 20-23 minutes)

## notes
1. allow to cool for about 10 minutes before slicing and serving
2. [from](https://cafedelites.com/easy-buttermilk-cornbread/)
