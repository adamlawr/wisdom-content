categories: cycling

# road

## ridley noah sl
- from: [mec](https://www.mec.ca/en/)
- on: 2019-01-03
- for: $3900
- components
  - group: ultegra
  - cassette: miche 11 speed, 52-36 x 12-34, strava kms: 12360

## devinci millenium
- from: [oak bay bikes](https://www.oakbaybikes.com)
- on: 2000?
- for: $2400
- components
  - ultegra
  - 9 speed

# cross

## broadie cx
- from: [cycleswest](http://www.cycleswest.ca)
- on: 2019-09-18
- for: $2500
- components
  - shimano 105
  - 11 speed, 38 x 11-32
  - long cage rear mech

## specialized crux
- from: [russhays](https://www.russhays.com)
- on: 
- for:
- components
  - sram rival
  - 10 speed, 46-36 x 11-34
  - long cage rear mech

# other

## cheese bike
- from: [used victoria](https://usedvictoria.com)
- on: 2018?
- for: $200
