categories: aws
date: 2017-11-01

# app deployment notes

# get application code
### make ssh key for bitbucket
`ssh-keygen` (~/.ssh/bitbucket)
is ssh-agent running? `ps -e | grep [s]sh-agent`
start it if not `eval ssh-agent $SHELL`
add the new key `ssh-add ~/.ssh/bb`
list ssh identities `ssh-add -l`. bitbucket should be there now

### upload the key to bitbucket
- copy: `cat ~/.ssh/bb.pub`
- create bitbucket key at: https://bitbucket.org/moops/{repo_name}/admin/access-keys/
- create github ket at: https://github.com/settings/keys

### get the code
```bash
cd ~/apps
git clone git@bitbucket.org:moops/{site_name}.git
cd {site_name}
git status
gem install bundler
bundle install
```

### configure environment variables

- get the output from `rails secret`
- create file at `/config/application.yml` with the following:

```
production:
  SMTP_USERNAME: zendogz3@gmail.com
  SMTP_PASSWORD: 3lf!naha7
  SECRET_KEY_BASE: <<output from `rails secret`>>
```

# build the database
### create a database user
`sudo -u postgres createuser {db_user} -P`

grant create db permission
```bash
sudo -u postgres psql postgres
postgres=# ALTER USER {db_user} CREATEDB;
postgres=# \q
```

### create config/database.yml
use this example or copy one from a fresh generation for a current version of rails:

```
default: &default
  adapter: postgresql
  encoding: unicode
  pool: <%= ENV.fetch("RAILS_MAX_THREADS") { 5 } %>
  timeout: 5000
  host: localhost

production:
  <<: *default
  database: {database_name}
  username: {db_user_from_last_step}
  password: {db_password_from_last_step}
```

### generate the database
`RAILS_ENV=production rails db:setup`

# configure the nginx host
- create a file at `/etc/nginx/sites-available/{site_name}` with the following:

```
server {
  listen 80 default_server;
  server_name {site_name}.raceweb.ca;
  passenger_enabled on;
  passenger_app_env production;
  root /home/ubuntu/apps/{site_name}/public;
}
```

- enable the new site
`sudo ln -s /etc/nginx/sites-available/{site_name} /etc/nginx/sites-enabled/{site_name}`

- retart nginx `sudo service nginx restart`

# compile assets
there does not appear to be a javascript runtime installed on ubuntu. nodejs is installed (`apt list | grep nodejs` to confirm) but ruby doesn't seem to have access to it.
- remove `gem coffee-rails` from the gemfile because we never use it anyhow.
- `bundle install` and `bundle list` to make sure it's gone

```
RAILS_ENV=production rails tmp:cache:clear
RAILS_ENV=production rails assets:clobber
RAILS_ENV=production rails assets:precompile
```

# app specific tasks
### photos
- connect `ssh -i "~/Development/rails/brainpad.pem" ubuntu@ec2-34-215-38-133.us-west-2.compute.amazonaws.com`
- edit `/etc/nginx/nginx.conf` to include `client_max_body_size 200M;` under the 'basic settings' section.
- retart nginx `sudo service nginx restart`

### brainpad
- connect `ssh -i "~/Development/rails/brainpad.pem" ubuntu@ec2-34-214-62-46.us-west-2.compute.amazonaws.com`
- had to install libxml2 with `sudo apt install libxml2` to get the `libxml-ruby` gem installed which is needed by the `twilio-ruby` gem
