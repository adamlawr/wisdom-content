categories: aws, devops
date: 2017-11-01

# aws server setup

# with chef/knife
install python if necessary

    $ python --version
install pip if necessary (it comes with recent versions of python)

    $ pip --version
install awscli [github](https://github.com/aws/aws-cli)

    $ pip install awscli

or updrade awscli with

    $ pip install --upgrade awscli

configure aws access

    $ aws configure

create the ec2 server with [knife-ec2](https://github.com/chef/knife-ec2)

    $ knife ec2 server create -I ami-51537029 --region us-west-2 -f t2.micro --aws-tag Name=test-3 --security-group_id sg-08a9e9ab712b50f99 --ssh-key raceweb_cgi --ssh-user ubuntu --identity-file raceweb_cgi.pem --local-mode

# ubuntu (16.04)
- connect `ssh -i "~/Development/rails/brainpad.pem" ubuntu@ec2-34-214-62-46.us-west-2.compute.amazonaws.com`
- out of the box ruby is not installed but git is
- `sudo apt-get update`
- `sudo apt-get install build-essential patch ruby-dev zlib1g-dev liblzma-dev libssl-dev libreadline-dev`
- set time zone `sudo timedatectl set-timezone Canada/Pacific`

# ruby

put this in `~/.gemrc` to avoid downloading unnecessary documentation:
`gem: --no-document`

### rbenv
    $ git clone https://github.com/rbenv/rbenv.git ~/.rbenv
    $ cd ~/.rbenv && src/configure && make -C src
    $ echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
    $ echo 'eval "$(rbenv init -)"' >> ~/.bashrc
    $ git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
    $ ~/.rbenv/bin/rbenv init
    $ bash --login # restart the shell
    $ type rbenv #=> "rbenv is a function"
    $ rbenv install 2.3.3
    $ rbenv global 2.3.3

### rvm
get updated commands from http://rvm.io
    $ gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
    $ \curl -sSL https://get.rvm.io | bash -s stable
    $ source /home/ubuntu/.rvm/scripts/rvm
    $ rvm -v
    $ rvm install 2.1.4

### rails
- `sudo gem install rails`

### bundler
- `sudo gem install bundler`

# passenger and nginx
- these instructions are from: https://www.phusionpassenger.com/library/install/nginx/install/oss/xenial/

```bash
# Install PGP key and add HTTPS support for APT
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 561F9B9CAC40B2F7
sudo apt-get install -y apt-transport-https ca-certificates

# Add APT repository
sudo sh -c 'echo deb https://oss-binaries.phusionpassenger.com/apt/passenger xenial main > /etc/apt/sources.list.d/passenger.list'
sudo apt-get update

# Install Passenger + Nginx
sudo apt-get install -y nginx-extras passenger
```

- Edit `/etc/nginx/nginx.conf`
- uncomment or add: `include /etc/nginx/passenger.conf;`
- retart nginx `sudo service nginx restart`
- check nginx status `systemctl status nginx.service`

check installation

    $ sudo /usr/bin/passenger-config validate-install
    $ sudo /usr/sbin/passenger-memory-stats

# mongodb
### <a id="user-content-mongo-install">install</a>
- `sudo apt install mongodb`
- copy the following to /etc/systemd/system/mongodb.service

```
[Unit]
Description=High-performance, schema-free document-oriented database
After=network.target

[Service]
User=mongodb
ExecStart=/usr/bin/mongod --quiet --config /etc/mongodb.conf

[Install]
WantedBy=multi-user.target
```
### start stop
- start mongodb `sudo systemctl start mongodb`
- verify it's started `sudo systemctl status mongodb`
- set it to run on system startup `sudo systemctl enable mongodb`
- stop mongodb (just in case) `sudo systemctl stop mongodb`

### initial import
these steps could be replaced by doing a [restore from s3](#user-content-restore-from-an-s3-backup-when-necessary) __or__:

put files on server
```bash
scp -i brainpad.pem ./brainpad/_backup_data/2016-11-26/workouts.json ubuntu@ec2-<CURRENT-IP>.us-west-2.compute.amazonaws.com:~/backup/2016-11-26
```
import data
```bash
mongoimport --db brainpad_production --collection workouts --file backup/2016-11-26/workouts.json --drop
...
```

# postgresql
### <a id="user-content-postgres-install">install</a>
- install: `sudo apt-get install postgresql postgresql-contrib`
- should already be installed and running. check with `sudo -u postgres psql postgres`
### dev library
- neccesary for `bundle install` for the pg gem
- `sudo apt-get install libpq-dev`

### <a id="user-content-postgres-service">service</a>
- `sudo /etc/init.d/postgresql start|stop|restart`

# other
### imagemagick
for photos app:
`sudo apt install imagemagick`
### dns
- https://www.domainsunder.ca
- need to update CNAME record if the ec2 instance restarts and changes IP address
- may need to flush local dns cache with: `sudo dscacheutil -flushcache; sudo killall -HUP mDNSResponder;`

### git aliases
- create `~/.gitconfig` with the following:
```
[alias]
  co = checkout
  st = status
  br = branch
  lo = log --color --graph --pretty=format:'%Cred%h%Creset %Cblue%an%Creset %C(yellow)%d%Creset %s %Cgreen(%cr)'
```

### schedule daily reminders summary
- from [cron with rails](https://medium.com/@pawlkris/scheduling-tasks-in-rails-with-cron-and-using-the-whenever-gem-34aa68b992e3)
- create `bin/send_reminders` with:

```
#!/bin/bash

cd /home/ubuntu/brainpad
RAILS_ENV=production rails send_reminders
```
- make it executable `chmod +x bin/send_reminders`
- schedule it `crontab -e`
- 7:05am every day `5 7 * * * 'cd /home/ubuntu/brainpad && bundle exec rails send_reminders' >> log/reminders.log`
- list to make sure: `crontab -l`

### git deploy hook
- [how to use git hooks to deploy](https://www.digitalocean.com/community/tutorials/how-to-use-git-hooks-to-automate-development-and-deployment-tasks)

### add a deploy user - DEPRECATED
REPLACE THESE STEPS WITH: http://www.ampedupdesigns.com/blog/show?bid=44
- create a user called deploy `sudo adduser --disabled-password deploy`
- add sudo to the new user `sudo usermod -aG sudo deploy`

```bash
su - deploy
mkdir .ssh
chmod 700 .ssh
touch .ssh/authorized_keys
```

- copy contents of public key from the .pem file of the keypair for the instance. paste it in the authorized_keys file you just created
- `vi .ssh/authorized_keys`


## nginx

### block all requests by default
change the file in `/etc/nginx/sites-enabled/default` to look like this:

```
server {
  listen 80 default_server;
  return 444;
}
```

then run this to restart and check the status

```
service nginx restart
nginx -t
```
