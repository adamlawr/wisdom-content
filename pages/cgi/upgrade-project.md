Categories: cgi
Date: 2018-05-01

# jogs project
## servers
### payara
- [home](https://www.payara.fish)
- [pre releases](https://www.payara.fish/upstream_builds)
- fork of, and drop in replacement for glassfish
- java 9 support since version 5 alpha 2

### glassfish
- [home](https://javaee.github.io/glassfish/)
- [pre release builds](http://download.oracle.com/glassfish/5.0.1/nightly)
- java 9 support planned in 5.0.1
- downloaded 5.0.1-b01-02_07_2018
### instructions
- [jee8 netbeans setup](https://github.com/javaee/j1-hol#initial-setup)

#### trick eclipse into installing glassfish 5
- change the manifest of the glassfish jar that eclipse uses
1 make sure `c:\temp` is empty (or change these instructions to use an empty dir)
2
```
$ cp [glassfish-install-dir]\glassfish\modules\glassfish-api.jar c:\temp
$ cd c:\temp
$ jar -xf glassfish-api.jar
$ atom META-INF\MANIFEST.MF
```
3 change the version on `Bundle-Version: 5.0.0` line to be `4.0.0`
4 re-jar and put it back (still in `c:\temp` here)
```
$ rm glassfish-api.jar
$ jar -cfm glassfish-api.jar META-INF\MANIFEST.MF .
$ mv [glassfish-install-dir]\glassfish\modules\glassfish-api.jar [glassfish-install-dir]\glassfish\modules\glassfish-api.jar.original
$ mv glassfish-api.jar [glassfish-install-dir]\glassfish\modules\glassfish-api.jar
```

### wildfly
- [download](http://wildfly.org/downloads/)


## ide
### eclipse
- [home](https://www.eclipse.org/)
- [docs](https://help.eclipse.org/oxygen/index.jsp)
- instructions [add support for gf 5 or payara 5](https://stackoverflow.com/questions/46584107/eclipse-support-for-glassfish-5/47365860#47365860)
### maven
1 windows -> preferences -> maven -> installations -> add
2 then browse to the directory where you extracted maven, save

### netbeans
- [home](https://netbeans.org/)
- [pre releases](http://bits.netbeans.org/download/trunk/nightly/latest)
instructions
- [jdk 9 support](http://wiki.netbeans.org/JDK9Support)

## java
### open jdk 9
- [home](http://openjdk.java.net/)
- [open jdk java se differences](https://developer.jboss.org/thread/277046)

## primefaces
- [home](https://www.primefaces.org/)


## moh apps

### dsam
- test: https://dsamt.hlth.gov.bc.ca/
