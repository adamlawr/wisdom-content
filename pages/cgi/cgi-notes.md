categories: cgi
date: 2018-05-01

# cgi notes

# git
- make sure you have it `$ git --version`
- configure:
```
$ git config --global alias.co checkout
$ git config --global alias.br branch
$ git config --global alias.st status
$ git config --global alias.lo log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue) <%an>%Creset' --abbrev-commit
```

## git svn
- change to the source folder
`$ cd C:\app\moh\_projects`
- create a git repo from an svn repo
- with everything:
`$ git svn clone -s --username atlawren https://subversion.hlth.gov.bc.ca/svn/{repo}`
- or with everything since revision 1234:
`$ git svn clone -s -r 1234:HEAD --username atlawren https://subversion.hlth.gov.bc.ca/svn/{repo}`
- or specify non-standard svn structure
`$ git svn clone --trunk=trunk --branches=branches --tags=tags --username atlawren https://subversion.hlth.gov.bc.ca/svn/{repo}`

- look at the branches you got with the clone
`$ git br -a`

- check out a branch
`$ git co -b jav1-17 remotes/origin/0.3.0`

- create a new svn branch
- this will be copied from the `origin/0.3.0` branch because that's the last svn branch that you referenced
`$ git svn branch jav1-17`

- switch your local git branch to the new svn branch
`$ git rebase remotes/origin/jav1-17`

- make sure you're pushing to the right branch
`$ git svn dcommit --dry-run`

- (haven't tried yet but looks promising
[svn utils](https://github.com/jonathancone/svn-utils)

# maven
## jdbc driver

    mvn install:install-file -Dfile={c:/app/ojdbc6.jar} -DgroupId=com.oracle -DartifactId=ojdbc6 -Dversion=11.2.0 -Dpackaging=jar

## dsam

    mvn install:install-file -Dfile=dejavu.jar -DgroupId=com.cgi.dsam -DartifactId=dejavu -Dversion=1.0 -Dpackaging=jar

## gis

    mvn install:install-file -Dfile=ASSMWebCommon.jar -DgroupId=com.cgi.gis -DartifactId=ASSMWebCommon -Dversion=1.0 -Dpackaging=jar
    mvn install:install-file -Dfile=hnweb_sso.jar -DgroupId=com.cgi.gis -DartifactId=hnweb_sso -Dversion=1.0 -Dpackaging=jar
    mvn install:install-file -Dfile=cgi-glassfish-custom-jndi-resource.jar -DgroupId=com.cgi.gis -DartifactId=cgi-glassfish-custom-jndi-resource -Dversion=1.0 -Dpackaging=jar
    mvn install:install-file -Dfile=errorhandler.jar -DgroupId=com.cgi.gis -DartifactId=errorhandler -Dversion=1.0 -Dpackaging=jar

### webcommon

    mvn install:install-file -Dfile=hnsc-sso-bin-330.jar -DgroupId=com.cgi.gis -DartifactId=hnsc-sso-bin-330 -Dversion=1.0 -Dpackaging=jar

## tap

    mvn install:install-file -Dfile=ASSMWebCommon.jar -DgroupId=com.cgi.tap -DartifactId=ASSMWebCommon -Dversion=1.0 -Dpackaging=jar
    mvn install:install-file -Dfile=cgi-glassfish-custom-jndi-resource.jar -DgroupId=com.cgi.tap -DartifactId=cgi-glassfish-custom-jndi-resource -Dversion=1.0 -Dpackaging=jar
    mvn install:install-file -Dfile=hnsc-sso-bin-330.jar -DgroupId=com.cgi.tap -DartifactId=hnsc-sso-bin-330 -Dversion=1.0 -Dpackaging=jar
    mvn install:install-file -Dfile=ojdbc8.jar -DgroupId=com.oracle -DartifactId=ojdbc8 -Dversion=12.0 -Dpackaging=jar

# fmdb

    mvn install:install-file -Dfile=crystal_reports_library/CrystalCharting.jar -DgroupId=com.crystaldecisions -DartifactId=CrystalCharting -Dversion=1.0 -Dpackaging=jar
    mvn install:install-file -Dfile=crystal_reports_library/CrystalContentModels.jar -DgroupId=com.crystaldecisions -DartifactId=CrystalContentModels -Dversion=1.0 -Dpackaging=jar
    mvn install:install-file -Dfile=crystal_reports_library/CrystalDatabaseConnectors.jar -DgroupId=com.crystaldecisions -DartifactId=CrystalDatabaseConnectors -Dversion=1.0 -Dpackaging=jar
    mvn install:install-file -Dfile=crystal_reports_library/CrystalExporters.jar -DgroupId=com.crystaldecisions -DartifactId=CrystalExporters -Dversion=1.0 -Dpackaging=jar
    mvn install:install-file -Dfile=crystal_reports_library/CrystalExportingBase.jar -DgroupId=com.crystaldecisions -DartifactId=CrystalExportingBase -Dversion=1.0 -Dpackaging=jar
    mvn install:install-file -Dfile=crystal_reports_library/CrystalFormulas.jar -DgroupId=com.crystaldecisions -DartifactId=CrystalFormulas -Dversion=1.0 -Dpackaging=jar
    mvn install:install-file -Dfile=crystal_reports_library/CrystalQueryEngine.jar -DgroupId=com.crystaldecisions -DartifactId=CrystalQueryEngine -Dversion=1.0 -Dpackaging=jar
    mvn install:install-file -Dfile=crystal_reports_library/CrystalReportEngine.jar -DgroupId=com.crystaldecisions -DartifactId=CrystalReportEngine -Dversion=1.0 -Dpackaging=jar
    mvn install:install-file -Dfile=crystal_reports_library/CrystalReportingCommon.jar -DgroupId=com.crystaldecisions -DartifactId=CrystalReportingCommon -Dversion=1.0 -Dpackaging=jar

    mvn install:install-file -Dfile=crystal_reports_library/jrcerom.jar -DgroupId=com.crystaldecisions -DartifactId=jrcerom -Dversion=1.0 -Dpackaging=jar
    mvn install:install-file -Dfile=crystal_reports_library/rasapp.jar -DgroupId=com.crystaldecisions -DartifactId=rasapp -Dversion=1.0 -Dpackaging=jar
    mvn install:install-file -Dfile=crystal_reports_library/rascore.jar -DgroupId=com.crystaldecisions -DartifactId=rascore -Dversion=1.0 -Dpackaging=jar
    mvn install:install-file -Dfile=crystal_reports_library/serialization.jar -DgroupId=com.crystaldecisions -DartifactId=serialization -Dversion=1.0 -Dpackaging=jar
    mvn install:install-file -Dfile=crystal_reports_library/webreporting.jar -DgroupId=com.crystaldecisions -DartifactId=webreporting -Dversion=1.0 -Dpackaging=jar

    mvn install:install-file -Dfile=attachment.jar -DgroupId=com.cgi.fmdb -DartifactId=attachment -Dversion=1.0 -Dpackaging=jar
    mvn install:install-file -Dfile=LoginModules.jar -DgroupId=com.cgi.fmdb -DartifactId=LoginModules -Dversion=1.0 -Dpackaging=jar

    mvn install:install-file -Dfile=errorhandler.jar -DgroupId=com.cgi.fmdb -DartifactId=errorhandler -Dversion=1.0 -Dpackaging=jar

## pcbl

    mvn install:install-file -Dfile=hnweb_sso.jar -DgroupId=com.cgi.pcbl -DartifactId=hnweb_sso -Dversion=1.0 -Dpackaging=jar

    application properties pretty sure not needed anymore:
    sso_hnweb_url=https://hnwd1.moh.hnet.bc.ca/
    sso_ldap_oid_user_role=pcbluserrole
    sso_application_name=pbld1
    sso_application_entry=/faces/Admin.xhtml


## miwt
old dependencies that might be needed at some point
```
<!-- <dependency>
    <groupId>ca.bc.gov.health</groupId>
    <artifactId>ASSMHNWebSso</artifactId>
    <version>1.0</version>
</dependency> -->
<!-- <dependency>
   <groupId>bouncycastle</groupId>
   <artifactId>bouncycastle-jce-jdk13</artifactId>
   <version>112</version>
</dependency> -->
<!-- <dependency>
   <groupId>org.bouncycastle</groupId>
   <artifactId>bcprov-jdk15on</artifactId>
   <version>1.60</version>
</dependency> -->
<!--
<dependency>
   <groupId>commons-logging</groupId>
   <artifactId>commons-logging</artifactId>
   <version>1.2</version>
</dependency> -->
```

# assm-auth
## assmauth-common

    mvn install:install-file -Dfile=hnweb_sso.jar -DgroupId=com.cgi.assmauth-common -DartifactId=hnweb_sso -Dversion=1.0 -Dpackaging=jar

## assmauth-ejb

## assmauth-war

    `cd ASSM-Common\ASSMWebCommon\lib\webui`
    mvn install:install-file -Dfile=webui.jar -DgroupId=com.cgi.assmauth-war -DartifactId=webui -Dversion=1.0 -Dpackaging=jar



com.sun.rave.web.ui.appbase

    get jar file from: http://bits.netbeans.org/nexus/content/groups/netbeans/org/netbeans/external/jsfcl/RELEASE65/

    install it with:

    mvn install:install-file -Dfile=c:\Users\adam.lawrence\Downloads\jsfcl-RELEASE65.jar -DgroupId=org.netbeans.external -DartifactId=jsfcl -Dversion=RELEASE65 -Dpackaging=jar

    use it with:

    <dependency>
        <groupId>org.netbeans.external</groupId>
        <artifactId>jsfcl</artifactId>
        <version>RELEASE65</version>
    </dependency>

    <!-- <dependency>
        <groupId>com.cgi.assmauth-war</groupId>
        <artifactId>webui</artifactId>
        <version>1.0</version>
    </dependency> -->

com.sun.webui.jsf.component.*

    <dependency>
        <groupId>com.sun.woodstock</groupId>
        <artifactId>webui-jsf</artifactId>
        <version>4.4.0.1</version>
    </dependency>

com.sun.data.provider.RowKey
com.sun.data.provider.impl.*

    <dependency>
        <groupId>com.sun.woodstock.dependlibs</groupId>
        <artifactId>dataprovider</artifactId>
        <version>1.0</version>
    </dependency>

# java
- search a rar file for a class

    jar -tvf com\sun\faces\jsf-spi\1.0\jsf-spi-1.0.jar | grep AbstractPageBean

## java console
### jdk 9 and above
try [jshell](https://en.wikipedia.org/wiki/JShell)

### jdk 8
use [this repl](https://github.com/albertlatacz/java-repl)
at CGI it's installed at `c:\app\personal\java\java-repl`
#### get build use
- `git clone https://github.com/albertlatacz/java-repl.git`
- `cd java-repl`
- `gradle shadowJar`
-  run it with `java -jar build/libs/javarepl-dev.jar`



# artifactory

## access
- [web access](https://artifactorydev.hlth.gov.bc.ca/artifactory/webapp)

- add the following to `~/.m2/repository/settings.xml` for maven access to artifactory

```
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 http://maven.apache.org/xsd/settings-1.0.0.xsd">
    <servers>
        <!-- dev -->
        <server>
            <id>artifactoryDevRepo</id>
            <username>admin</username>
            <password>blah123blah</password>
        </server>
        <!-- prod -->
        <server>
            <id>artifactoryRepo</id>
            <username>admin</username>
            <password>!likeDevOps</password>
        </server>
    </servers>
    <profiles>
        <!-- dev -->
        <profile>
            <id>artifactoryDevProfile</id>
            <repositories>
                <repository>
                    <id>artifactoryDevRepo</id>
                    <name>artifactory dev repo</name>
                    <url>https://artifactorydev.hlth.gov.bc.ca/artifactory/test-pom-local</url>
                </repository>
            </repositories>
        </profile>
        <!-- prod -->
        <profile>
            <id>artifactoryProfile</id>
            <repositories>
                <repository>
                    <id>artifactoryRepo</id>
                    <name>artifactory repo</name>
                    <url>https://artifactory.hlth.gov.bc.ca/artifactory</url>
                </repository>
            </repositories>
        </profile>
    </profiles>
    <activeProfiles>
        <activeProfile>artifactoryDevProfile</activeProfile>
        <!--
        <activeProfile>artifactoryProfile</activeProfile>
        -->
    </activeProfiles>
</settings>
```

## certificate
- get a certificate from the mid-tier team. I got `c:\app\moh\artifactory-cert.txt` in April 2018
- list jdk certificates, note the number. (the quotes are only necessary if there's a space in `$JAVA_HOME`)
`$ keytool -list -keystore "%JAVA_HOME%/jre/lib/security/cacerts"`

- the keystore default password is `changeit`

- add the artifactory certificate chain (the quotes are only necessary if there's a space in `$JAVA_HOME`)
`$ keytool -import -trustcacerts -file C:\app\_clients\moh\artifactory-cert.txt -alias MOH_ARTIFACTORY -keystore "%JAVA_HOME%/jre/lib/security/cacerts"`
`$ keytool -import -trustcacerts -file C:\app\_clients\moh\artifactorydev.hlth.gov.bc.ca.crt.txt -alias MOH_DEV_ARTIFACTORY -keystore "%JAVA_HOME%/jre/lib/security/cacerts"`

# glassfish
## deploy
`$ asadmin deploy --deploymentplan "[ABSOLUTE_PATH_TO_YOUR_GLASSFISH_RESOURCE_JAR]" --name=surf --force=true --createtables=false "C:\app\moh\_projects\SURF\surf\surf-ear\target\surf-ear.ear"`
## list log levels
`$ asadmin list-log-levels`


## glassfish on MOH app servers
- dev: cinnamon
- eg. `$ tail -100f /s01/ogs/HNI-DAS/glassfish/nodes/HNI_CINNAMON_NODE1/HNWeb-Dev/logs/server.log`

# virtualbox
- log into the docker host and make a mount point
```
docker-machine ssh
sudo mkdir /mnt/data
sudo mount -t vboxsf data /mnt/data
```

# docker

# start an interactive shell in a running container
`docker exec -it pg psql -U postgres`
`docker exec -it brainpad_db_1 bash`

copy a local file to a docker container
`docker cp ./hem.sql pg:/tmp`
`docker exec -it pg psql -U postgres -f /tmp/hem.sql`

## oracle
[oracle-12c docker image](https://hub.docker.com/r/sath89/oracle-12c/)


# eclipse
- source attacher plugin. add this link to the 'install software' option in the help menu
[bintray](https://dl.bintray.com/tha/eclipserepo/)

## eclipse / payara plugin
clone [eclipse-glassfish-plugin](https://github.com/payara/ecosystem-eclipse-glassfish-plugin)
and build it with ant
note: the ant build needs `JDK_8_HOME` environment variable set for some stupid reason.
imagine needing a separate environment variable to point to each version of the JDK instead of just reading `JAVA_HOME`.
the java ecosystem is stupid
```
$ cd c:\app\servers
$ git clone https://github.com/payara/ecosystem-eclipse-glassfish-plugin
$ cd ecosystem-eclipse-glassfish-plugin
$ ant
```
drag the zip file created in `./build/packages` to the 'install software' window in eclipse.
something like 'payare tools' should be available to install.

# jetty
- [home](https://www.eclipse.org/jetty/)
- [download](https://www.eclipse.org/jetty/download.html)
- [documentation](https://www.eclipse.org/jetty/documentation/9.4.14.v20181114/)
unzip to `c:\app\servers\jetty-xx`

set env:
 ```
$ set JETTY_HOME=c:\app\servers\jetty-xx
$ set JETTY_HOME=c:\app\servers\jetty-xx\demo-base
 ```

start like this:
```
$ cd $JETTY_BASE
$ java -jar $JETTY_HOME/start.jar jetty.http.port=8081
```

# payara
start/stop domain
```
$ asadmin start-domain
$ asadmin stop-domain
```
change admin password (no password by default)
```
asadmin change-admin-password
```

# jrebel
## add jvm option to GlassFish
- localhost:4848 > Configurations > default config > JVM Settings > JVM Options > add JVM option:
`-Djava.ext.dirs=${com.sun.aas.javaRoot}/lib/ext${path.separator}${com.sun.aas.javaRoot}/jre/lib/ext${path.separator}${com.sun.aas.instanceRoot}/lib/ext`


# servers
## sso database
sneaker.moh.hnet.bc.ca
1521
ssod1
ssoclient

## fireblade
dev app server
ssh atlawren@fireblade.hlth.gov.bc.ca
### logs
/data/gfadmin/software/payara5/glassfish/nodes/DEV_Fireblade_Node/DSAM-Dev-1/logs
/data/gfadmin/software/payara5/glassfish/nodes/DEV_Fireblade_Node/EMACCS-Dev-1/logs
/data/gfadmin/software/payara5/glassfish/nodes/DEV_Fireblade_Node/FMDB-Dev-1/logs
/data/gfadmin/software/payara5/glassfish/nodes/DEV_Fireblade_Node/GIS-Dev-1/logs
/data/gfadmin/software/payara5/glassfish/nodes/DEV_Fireblade_Node/MIWT-Dev-1/logs
/data/gfadmin/software/payara5/glassfish/nodes/DEV_Fireblade_Node/PCBL-Dev-1/logs
/data/gfadmin/software/payara5/glassfish/nodes/DEV_Fireblade_Node/SWT-Dev-1/logs
/data/gfadmin/software/payara5/glassfish/nodes/DEV_Fireblade_Node/TAP-Dev-1/logs

## ruckus
dev das server

## daudlin
dev db server

mid (miwt)
```
MIWTD12c.WORLD =
  (DESCRIPTION =
    (ADDRESS = (COMMUNITY = TCP.world)(PROTOCOL = TCP)(HOST = daudelin.hlth.gov.bc.ca)(PORT = 1521))
    (CONNECT_DATA =
      (SID = miwtd)
    )
  )
```

surfd
```
SURFD12c.WORLD =
  (DESCRIPTION =
    (ADDRESS = (COMMUNITY = TCP)(PROTOCOL = TCP)(HOST = daudelin.hlth.gov.bc.ca)(PORT = 1521))
    (CONNECT_DATA =
      (SID = surfd)
    )
  )
```

## tondino
test database server

```
WEBT12.WORLD =
  (DESCRIPTION =
    (ADDRESS = (PROTOCOL = TCP)(HOST = tondino.hlth.gov.bc.ca)(PORT = 1521))
    (CONNECT_DATA =
      (GLOBAL_NAME = WEBT.HLTH.GOV.BC.CA)
      (SID = webt)
    )
  )
```

```
FMDT.WORLD =
  (DESCRIPTION =
    (ADDRESS = (PROTOCOL = TCP)(HOST = tondino.hlth.gov.bc.ca)(PORT = 1521))
    (CONNECT_DATA =
      (GLOBAL_NAME = fmdt.world)
      (SID = fmdt)
    )
  )
```

## turron

```
SWTD12c.world =
  (DESCRIPTION =
    (ADDRESS = (COMMUNITY = TCP)(PROTOCOL = TCP)(HOST = turron.hlth.gov.bc.ca)(PORT = 1521))
    (CONNECT_DATA =
      (SID = swtd)
    )
  )
```


## bouncycastle upgrade attempts
```
<dependency>
    <groupId>org.bouncycastle</groupId>
    <artifactId>bcprov-jdk15on</artifactId>
    <version>1.61</version>
</dependency>
<dependency>
    <groupId>org.bouncycastle</groupId>
    <artifactId>bcpkix-jdk15on</artifactId>
    <version>1.58</version>
</dependency>
```

http://localhost:8080/MedicalImaging/?sendingInstance=hnwd2&token=AC279A749C95F11D6281D40F7D7C32CE4066285495299C397961D2745EC24F51DFFF9FCD120583A6BDC3E920A7239429B65CFE16CDA19C52CAE6ED79CED5030152394610F306EB88982FCA6F0F714895&stoken=2CF36957AD96F98C43B6A67D510215379B4D24968BB7D625736D78C8AA0F00F284140FC36D2AF76C9AE46E5DDF2DA74CAC9A848A41A4446C0A69346DD584AB8CEE7CA0A6F772439A0DFE80A20A340783F21CF0FBA0420FCC9FA41F51063B24BD627ACC9888C649494269CBC72BADBF5C77E9060D865ED35899432560D93BC0F4&ekey=0B3AC2FEDB822106B8B3596864A8A07657305501164BEF14DA700B392E1FA4795485CCFF199BDBC0C97F8FCD88811F09477940B5048E5EDBA3BCA50288753118CB93A344776AB4C914F7DF321BAD45629AC96F27A7BE9EBD05509E2712118081BED052D4D67598685766D20F9D1CFF665AF4D72A36190B89AFEDF24FCC4FB790

