categories: development

# css tips

most of my how tos are at [code pen](https://codepen.io/adamlawr/pens/public)

## sticky footer
html:

    <body>
      <div class="content">
        content
      </div>
      <footer class="footer"></footer>
    </body>

sass:

    html, body
      height: 100%
    body
      display: flex
      flex-direction: column
    .content
      flex: 1 0 auto
    .footer
      flex-shrink: 0
