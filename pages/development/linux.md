categories: development, devops

# linux
## os version

    cat /etc/os-release

## disk usage

for a direcory

    $ du -hs /path/to/dir

`-h` human readable
`-s` summary instead of listing all subdirectories

list the 100 biggest directories

    $ du -x -h / | sort -n | tail -100

`-x` skip directories on different file systems

## apt

- show cache locations: `$ apt-config dump | grep "^Dir\( \|::Ca\)"`
- show cache size: `sudo du -sh /var/cache/apt`
- clean only obsolete: `apt-get autoclean`
- see what will be removed: `apt-get clean --dry-run`
- remove: `apt-get clean`

## disable password logins
- [from here](https://itstillworks.com/disable-password-authentication-ubuntu-8289431.html)
- [and here](https://www.cyberciti.biz/faq/how-to-disable-ssh-password-login-on-linux/)

edit the ssh config

    $ vi /etc/ssh/sshd_config

- find `PasswordAuthentication` line and set to 'no'
- find `ChallengeResponseAuthentication` line and set to 'no'
- find `UsePAM` line and set to 'no'

restart the ssh service

    $ service sshd restart


## scp
download without password

    $ scp -i "/path/to/key.pem" user@host:/path/to/file ./save/here

upload without password

    $ scp -i "/path/to/key.pem" /source/file user@host:/save/here

## flush dns
- osx: `sudo dscacheutil -flushcache;sudo killall -HUP mDNSResponder`
- linux ``

## ssh

### ssh copy

- [ssh-copy-id](https://www.ssh.com/ssh/copy-id)

### server config

    vi /etc/ssh/sshd_config

add line for allowed users: `AllowUsers adam`

edit line to disallow root logins: `PermitRootLogin no`

edit line to change port: `Port 222` (22 is the default)

### client config

- [using-the-ssh-config-file](https://linuxize.com/post/using-the-ssh-config-file/)

```bash
$ touch ~/.ssh/config
$ chmod 600 ~/.ssh/config
```

```
Host <nickname>
    Hostname <ip or hostname>
    User <user>
    Port <default 22>
    IdentityFile /Users/adam/.ssh/linode_rsa
```

connect with

    $ ssh nickname

## users

list users

    $ cat /etc/passwd

- [create users with useradd](https://linuxize.com/post/how-to-create-users-in-linux-using-the-useradd-command/)
- [create users with adduser](https://linuxize.com/post/how-to-add-and-delete-users-on-ubuntu-18-04/)

add a group to an existing user

    $ sudo usermod -aG groupName userName

monitor logins

    $ tail -fn 200 /var/log/auth.log
    $ tail -fn 500 /var/log/nginx/access.log

# services

show running services

    $ systemctl | grep running

show the status of one of those services

    $ systemctl status httpd

# ssh ngrok tunnel
i haven't tried this but it looks interesting
If you have a web server somewhere, you can roll your own ngrok using SSH port forwards and an nginx reverse proxy on the server.

Bash shell alias for your localhost:

```bash
tunup () {
    port=${1:-5000}
    echo "Forwarding web:5000 to local port $port"
    ssh -R 5000:127.0.0.1:$port name@webserver.tld
}
```

And an nginx config for e.g. tunnel.webserver.tld that does a proxy_pass to the web server's localhost:5000:

```
server {
    server_name tunnel.webserver.tld;
    location / {
        proxy_pass http://localhost:5000
    }
}
```

Then whenever you want to open the ngrok tunnel run a command like "tunup" (port 5000) or "tunup 8080" if a different local port, and access your tunnel.webserver.tld domain. When the tunnel is NOT active, your domain will return an nginx 503 Bad Gateway error.
