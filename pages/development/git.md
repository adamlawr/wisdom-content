categories: development

## aliasses

    $ git config --global alias.co checkout
    $ git config --global alias.br branch
    $ git config --global alias.st status
    $ git config --global alias.lo "log --color --graph --pretty=format:'%Cred%h%Creset %Cblue%an%Creset %C(yellow)%d%Creset %s %Cgreen(%cr)'"

### move a directory to its own repo

[git-filter-branch](https://git-scm.com/docs/git-filter-branch)

    $ git clone <old-repo-remote> <new-repo-folder>
    $ cd <new-repo-folder>
    $ git filter-branch --subdirectory-filter <folder-to-export> -- --all
    $ git remote rm origin
    $ git remote add origin <new-repo-remote>
    $ git push --tags origin master
