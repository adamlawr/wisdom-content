categories: development

## links
- [ansible community](https://www.ansible.com/community)
- [ansible user guide](https://docs.ansible.com/ansible/latest/user_guide/index.html#)

## examples

provide a var on the command line
    
    $ ansible-playbook -e label=apps-3 linode/delete.yml


## certbot / let's encrypt

- [ansible playbook](https://graspingtech.com/ansible-lets-encrypt/)
