categories: development

# cloud providers

## vultr
[pricing](https://www.vultr.com/pricing/)

## digital ocean
[pricing](https://www.digitalocean.com/pricing/)

## aws
[pricing](https://aws.amazon.com/ec2/pricing/)

## hetzner
[pricing](https://www.hetzner.com/cloud#pricing)
- only european locations for now

## launch vps
[pricing](https://launchvps.com/index.html#pricing)

## liquid web
[pricing](https://www.liquidweb.com/products/vps/#pricing)
- way over priced. maybe i'm missing something

## google cloud
[pricing](https://cloud.google.com/compute/pricing)
