categories: development

# let's encrypt

- [letsencrypt docs](https://letsencrypt.org/docs/)
- [certbot ubuntu/nginx](https://certbot.eff.org/lets-encrypt/ubuntufocal-nginx)
- [tutorial](https://www.digitalocean.com/community/tutorials/how-to-secure-nginx-with-let-s-encrypt-on-ubuntu-20-04)
- [ssl test](https://www.ssllabs.com/ssltest/analyze.html?d=zendogz-stage.raceweb.ca)
- [ansible playbook](https://graspingtech.com/ansible-lets-encrypt/)

## install certbot

### snap

    $ snap install --classic certbot
    $ ln -s /snap/bin/certbot /usr/bin/certbot

### apt (prefered)

    $ apt install certbot python3-certbot-nginx

## get a cert

    certbot --nginx -d www.zendogz.ca
