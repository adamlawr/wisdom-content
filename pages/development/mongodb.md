categories: development

## links
- [documentation](https://docs.mongodb.com/manual/)
- [robo 3t](https://robomongo.org)

## robo3t
query UI

### query examples

    db.getCollection('contacts').find({ bd: '' }).count();
or
    db.contacts.find({ bd: '' }).count();

## mongo shell

### connect

    $ mongo --username brainpad --password

### show stuff

    $ show dbs
    $ use [db]
    $ show collections

### bulk update

    $ var bulk = db.contacts.initializeUnorderedBulkOp();
    $ bulk.find({ bd: '' }).update({ $set: { bd: null } });
    $ bulk.execute();
