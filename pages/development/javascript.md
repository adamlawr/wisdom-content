categories: development

# javascript
## vanilla javascript

- [gomakethings](https://gomakethings.com]
- [learn js](https://www.javascript.com/learn)

### page loaded

should only be necessary from head. if js is in the body, the dom should already be loaded

```
document.addEventListener("DOMContentLoaded", function() {
  console.log('dom loaded');
});
```

### fetch

- [spec](https://fetch.spec.whatwg.org)
- [mdn fetch](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API)
- [playground example](https://gitlab.com/adamlawr/playground/-/tree/master/javascript/ajax)
- [gomakethings](https://gomakethings.com/how-to-use-the-fetch-api-with-vanilla-js/)

### cors

- [wikipedia](https://en.wikipedia.org/wiki/Cross-origin_resource_sharing)
- [enable-cors](https://enable-cors.org/index.html)
- [playground example](https://gitlab.com/adamlawr/playground/-/tree/master/javascript/ajax)
