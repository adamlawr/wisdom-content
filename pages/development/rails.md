categories: development

## template examples:
- [https://github.com/dennybritz/rails_startup_template](https://github.com/dennybritz/rails_startup_template)
- [https://github.com/lab2023/rails-template](https://github.com/lab2023/rails-template)
- [https://github.com/greendog99/greendog-rails-template](https://github.com/greendog99/greendog-rails-template)

```
    # gems
    gem 'cancan'               # authorization (https://github.com/ryanb/cancan)
    gem 'simple_form'          # form builder (https://github.com/plataformatec/simple_form)
    gem 'kaminari'             # pagination
    gem 'redcarpet'            # markdown
    gem "bcrypt-ruby"          # for encrypted password

    gem_group :development do
    gem "rspec-rails"        # tests (https://github.com/rspec/rspec-rails)
    gem "debugger"
    end

    gem_group :test do
    gem "rspec-rails"        # tests (https://github.com/rspec/rspec-rails)
    gem "debugger"
    gem "factory_girl_rails" # instead of rails fixtures (https://github.com/thoughtbot/factory_girl)
    end

    gem_group :production do
    gem "rails_12factor"     # rails 4 deployment on heroku
    end


    # install gems and build database

    run 'bundle install'
    rake 'db:drop'
    rake 'db:create'
    rake 'db:migrate'


    # install bootstrap (3.0.0) from https://github.com/twbs/bootstrap

    if yes?("Download bootstrap?")
    run "curl https://codeload.github.com/twbs/bootstrap/zip/master -o bootstrap.zip"
    run "unzip bootstrap.zip -d bootstrap && rm bootstrap.zip"
    run "cp bootstrap/bootstrap-master/dist/css/bootstrap.css vendor/assets/stylesheets/"
    run "cp bootstrap/bootstrap-master/dist/js/bootstrap.js vendor/assets/javascripts/"
    run "rm -rf bootstrap"
    run "echo '@import \"bootstrap\";' >> app/assets/stylesheets/application.css.scss"
    end

    # initialize cancan

    say 'initializing cancan...'
    generate 'cancan:ability'

    # initialize simple_form

    say 'initializing simple_form...'
    generate 'simple_form:install --bootstrap'


    # tell git what to ignore

    run "cat << EOF >> .gitignore
    /.bundle
    config/initializers/secret_token.rb
    config/database.yml
    /log/*.log
    /tmp
    .DS_Store
    EOF"


    # initialize git
    # ==================================================
    git :init
    git add: "."
    git commit: %Q{ -m 'initial commit' }


    # bitbucket repository
    # ==================================================
    if yes?("create bitbucket repository?")
    git_uri = `git config remote.origin.url`.strip
    if git_uri.size == 0
        username = ask "what is your bitbucket username?"
        run "curl -u #{username} -d \"name=#{app_name}&is_private=true&language=ruby\" https://bitbucket.org/api/1.0/repositories"
        git remote: %Q{ add origin https://#{username}@bitbucket.org/#{username}/#{app_name}.git }
        git push: %Q{ origin master }
    else
        say "repository already exists:"
        say "#{git_uri}"
    end
    end
```

