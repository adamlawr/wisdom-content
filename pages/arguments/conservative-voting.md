categories: arguments

# bill c-6 (conversion therapy)

The bill would create five new Criminal Code offences relating to conversion therapy. These are:

- Causing a person to undergo conversion therapy against the person’s will;
- Causing a minor to undergo conversion therapy;
- Doing anything for the purpose of removing a minor from Canada with the intention that the minor undergo conversion therapy outside Canada;
- Advertising an offer to provide conversion therapy; and
- Receiving a financial or other material benefit from the provision of conversion therapy.

## links
- [summary](https://lop.parl.ca/sites/PublicWebsite/default/en_CA/ResearchPublications/LegislativeSummaries/432C6E)
- [votes by party](https://www.ourcommons.ca/Members/en/votes/43/2/175?view=party)

## conservative concerns

[Dane Loyd](https://openparliament.ca/bills/43-2/C-6/)

    Going by the very definition the government has included in the legislation, we are asked to accept that even discouraging someone from “non-heterosexual attraction or sexual behaviour or non-cisgender gender expression” is a criminal act of conversion therapy.


