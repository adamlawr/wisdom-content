# 2023 california

## rides
[best rides around la](https://www.komoot.com/guide/1935167/cycling-around-los-angeles)

[sdbc saturday](https://www.sdbc.org/saturday/)
    - UC Cyclery  8715 Villa La Jolla Dr, La Jolla, CA.
    - UC Cyclery is located in the Northwest corner of the La Jolla Village Square mall next to Union Bank
## bike shops
san diego
- [uccyclery](https://www.uccyclery.com)

### nacimiento-fergusson road
just south of lucia at the south end of big sur on highway 1
- [Nacimiento-Fergusson Road](http://bestrides.org/nacimiento-fergusson-road/)

### mt baldy
- [phil](https://www.youtube.com/watch?v=0NAk5WGawbA)

### san diego
- [sandiego climbs](https://pjammcycling.com/zone/23.San%20Diego%20County)
- [best one](https://pjammcycling.com/climb/153.Palomar%2520Mountain%2520South%2520Grade)


