categories: trips
date: 2020-01-30

# oakland 2020 february
## when
2020-02-15 - 2020-02-22

## bart
- [bart map](https://www.bart.gov/system-map)
- [bart stations](https://www.bart.gov/stations)
  - west oakland station (center st, 7th st)
  - embarcadero station (pine st, davis st)
- [bay bridge bike shuttle](https://dot.ca.gov/caltrans-near-me/district-4/d4-popular-links/crossing-thebay-by-bike)

## strava routes
- [around the bay](https://www.strava.com/routes/23653946) - 150km - finish at fishermans warf
- [mt diablo](https://www.strava.com/routes/23652707) - 42km - airbnb to the top of mt diablo
- [north oakland](https://www.strava.com/routes/23652951) - 118km - north oakland loop
- [muir woods](https://www.strava.com/routes/23653642) - 112km - loop north of the golden gate

## airbnb
- [orinda ca](https://www.airbnb.ca/rooms/8386950)
