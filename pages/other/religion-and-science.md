categories: other

# science and religion
## christians priority bias:
politicians not even trying to deny that they won't rule as christians

- [ted cruz](http://news.yahoo.com/ted-cruz-tells-reporters-im-201000179.html)
- [ted cruz 2](http://www.dailykos.com/story/2016/1/22/1473694/-Ted-Cruz-I-m-a-Christian-first-American-second-Imagine-if-Muslim-or-Jewish-politician-said-that)
- [republicans](http://www.salon.com/2016/02/02/make_them_talk_about_evolution_why_wont_a_single_republican_presidential_candidate_admit_that_darwins_right/)

## christian political influence
### climate change
this isn't specifically about religion. this is about fake news and spin. this is from a wings night debate about human caused climate change. the alberta crew brought up evidence that polar bear populations were increasing. I was skeptical but said I would look it up. The most popular google search link is for polarbearscience.com. the site seems wrong so I searched for where they got their funding to try to find biases. the next link explains it perfectly in the context of the larger problem of fake news and bias confirmation.

- [fake news](https://scholarsandrogues.com/2017/01/13/how-to-know-what-to-trust-in-the-age-of-fake-news/)
- [polar bear population](https://polarbearscience.com)

### gay marriage
- [jehovahs witnesses](http://www.gaystarnews.com/article/jehovahs-witnesses-released-bizarre-disturbing-video-teaching-kids-homophobic/#gs.vsDLUX0)

### lgbtq
- [just fuck you](https://www.opendemocracy.net/en/5050/us-christian-right-group-hosts-anti-lgbt-training-african-politicians/)
    Family Watch International, a US Christian conservative organisation has been coaching African politicians and religious leaders to oppose comprehensive sexuality education across the continent.

### abortion

### gun control
- since sandy hook, 12 states have made laws that people can carry guns in schools

### evolution
- [scary shit](https://www.youtube.com/watch?v=x2xyrel-2vI)
- [alabama biology](http://www.rawstory.com/2016/03/alabama-biology-textbooks-will-include-a-disclaimer-telling-students-to-doubt-evolution/#.VurTqjcmu6k.reddit)
- [kansas](http://www.rawstory.com/2016/04/court-smacks-down-kansas-christians-for-labeling-evolution-a-religion-to-force-school-ban/)
- [arizona young earth creationist](https://madmikesamerica.com/2018/09/arizona-asked-a-young-earth-creationist-to-review-evolution-teaching-standards/)
> A Young Earth Creationist who says he was called by God to “teach from the Biblical creation perspective” is part of a team in charge of reviewing Arizona’s science standards for teaching evolution. To the surprise of absolutely no one, it’s not going well

### vaccines

### birth control

## dumbing down
- [some religious references](http://www.macleans.ca/politics/america-dumbs-down/)
- [more dumbing down](http://www.salon.com/2016/09/30/idiocracy-now-donald-trump-and-the-dunning-kruger-effect-when-stupid-people-dont-know-they-are-stupid/) what can be allowed to happen if the population hasn't had a good education
- [contrast 1912 grade 8 exam](http://www.infowars.com/newly-discovered-eighth-grade-exam-from-1912-shows-how-dumbed-down-america-has-become/)
- [anti-science](http://www.huffingtonpost.com/victor-stenger/rising-antiscience-faith_b_3991677.html)
the third to last paragraph is excellent. especially this:
> In 2009, John Shimkus, Republican of Illinois, and a member of the Energy and Commerce Committee of the U.S. House of Representatives, argued that climate change is a myth because God told Noah he would never again destroy Earth by flood (Gen 8:21-22). He is seen on video as saying, "The earth will end only when God declares it's time to be over. Man will not destroy this earth. This earth will not be destroyed by a flood. . . . I do believe God's word is infallible, unchanging, perfect."

- [why people deny](https://www.theatlantic.com/ideas/archive/2019/01/what-deniers-climate-change-and-racism-share/579190)

## history
- [history of christianity's conflict with science](http://www.seesharppress.com/20reasons.html#numbereight)

## other
- [republicans as a danger to humanity](http://www.huffingtonpost.com/entry/noam-chomsky-gop_us_56a66febe4b0d8cc109aec78?utm_hp_ref=politics)
> it's not enough to say "if americans vote in these guys, they deserve what they get". the reason these guys are leading in polls is because the electorate are not educated and don't recognize the dangers of these proposed policies.

- [brain damage related to religious fundamentalism](https://www.alternet.org/2019/01/scientists-have-established-link-between-brain-damage-and-religious-fundamentalism/)
> Religious beliefs can be thought of as socially transmitted mental representations that consist of supernatural events and entities assumed to be real. Religious beliefs differ from empirical beliefs, which are based on how the world appears to be and are updated as new evidence accumulates or when new theories with better predictive power emerge. On the other hand, religious beliefs are not usually updated in response to new evidence or scientific explanations, and are therefore strongly associated with conservatism.

- [believing without evidence is morally wrong](https://getpocket.com/explore/item/believing-without-evidence-is-always-morally-wrong)
> Clifford’s argument might have been hyperbole when he first made it, but is no longer so today

# just religious bullshit

-[gender equality related to religiosity](https://theswaddle.com/men-are-less-religious-in-countries-with-more-gender-equality-shows-study/)

## mormons
- [sam young](http://www.kuer.org/post/excommunication-still-stands-sam-young-mormon-church-says#stream/0)
- [prop 8 new york times](https://www.nytimes.com/2008/11/15/us/politics/15marriage.html)
- [prop 8](https://www.dailydot.com/irl/mormon-church-gay-marriage-prop-8/)
- [kids with gay parents](https://friendlyatheist.patheos.com/2018/08/11/mormon-church-cements-policy-limiting-baptisms-for-kids-of-same-sex-parents/)
- [kids with gay parents](https://www.nytimes.com/2015/11/07/us/mormons-gay-marriage.html)
- [racism](https://www.nytimes.com/2012/08/19/opinion/sunday/racism-and-the-mormon-church.html?_r=0)
> It was Smith’s successor, Brigham Young, who adopted the policies that now haunt the church. He described black people as cursed with dark skin as punishment for Cain’s murder of his brother. “Any man having one drop of the seed of Cane in him cannot hold the priesthood,” he declared in 1852. Young deemed black-white intermarriage so sinful that he suggested that a man could atone for it only by having “his head cut off” and spilling “his blood upon the ground.”

- [racism](https://en.wikipedia.org/wiki/Black_people_and_Mormonism)
> Church publications from 2003 still recommended that young people marry those with similar racial backgrounds

## jehovah's witness
- [abuse coverup](https://www.theatlantic.com/family/archive/2019/03/the-secret-jehovahs-witness-database-of-child-molesters/584311/?rss=1)

## scientologist

## seventh day adventist

## catholic
