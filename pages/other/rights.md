categories: other

# rights

## good summary of american discrimination laws with examples
-[/www.cnn.com/2018/06/29](https://www.cnn.com/2018/06/29/us/when-businesses-can-deny-you-service-trnd/index.html)

## 'Brash, loud and obnoxious gay male' doesn't have right to rude emails, tribunal rules
- [www.cbc.ca/news](http://www.cbc.ca/news/canada/british-columbia/brash-loud-and-obnoxious-gay-male-doesn-t-have-right-to-rude-emails-tribunal-rules-1.4538080)

> Over the years, Colbert has sent hundreds of these off-colour messages to officials at the District of North Vancouver, prompting council to adopt a policy that redirects all email from people who have a history of sending threatening and inappropriate messages to the municipal clerk.
> But even that didn't dissuade Colbert, who was the only person covered by this policy.
> He turned to the B.C. Human Rights Tribunal, filing a complaint that alleged the municipal government was discriminating against him because he is gay and has been an advocate for LGBTQ issues.
> He described himself as a "brash, loud and obnoxious gay male," and said the district was trying to avoid oversight and input from its citizens, according to a tribunal decision.


# Cabbie lied about why he refused blind man's ride, admits 'I don't like dogs' to tribunal
- [www.cbc.ca/news](http://www.cbc.ca/news/canada/british-columbia/cabbie-lied-about-why-he-refused-blind-man-s-ride-admits-i-don-t-like-dogs-to-tribunal-1.4615367)

> ... they were waiting outside with Belusic's guide dog, a yellow lab named Birch, when they got an automated phone call to say their taxi had arrived. Belusic testified that he heard a car pull up, then drive away.
>
> In his defence, driver Gurdeep Dhesi maintained that he cancelled the ride because his son had called and needed to be picked up.
> Dhesi had claimed his son contacted him by cellphone, but when he was ordered to produce call records to prove that, it turned out his son didn't even own a cellphone.
>
> In a letter to the tribunal earlier this year, Dhesi admitted, "I chose not to pick up Mr. Belusic in the afternoon of December 29, 2016 because I don't like dogs."

# pharmasist insults women for prescription
[cnn.com/arizona-prescription-walgreens-miscarriagel](https://www.cnn.com/2018/06/25/health/arizona-prescription-walgreens-miscarriage/index.html)

> Walgreens says it allows pharmacists to step away from filling a prescription "for which they have a moral objection," but that they are required to "refer the prescription to another pharmacist or manager on duty to meet the patient's needs in a timely manner." The woman, Nicole Mone Arteaga, said she was later able to pick up her prescription from another Walgreens store.

# oil research
## The Fatal Flaw of Alberta’s Oil Export Expansion
- [theenergymix.com](http://theenergymix.com/2018/03/04/exclusive-out-of-the-loop-the-fatal-flaw-of-albertas-oil-export-expansion/)

> ...Dirty bitumen becomes a “product” or a “resource”. How sanitary. Blowing a hole in Canada’s national carbon budget as pledged under the Paris climate agreement becomes a “nation-building” crusade. A tax to help cut carbon pollution is a brave, virtuous policy—but only if tar sands/oil sands output can accelerate, and the tax rate is carefully calibrated to not put a dent in that. Fossil producers must pay royalties to help a debt-laden province—but are allowed to pay in barrels of raw bitumen instead of cash.
