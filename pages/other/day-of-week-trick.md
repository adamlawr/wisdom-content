categories: other

# day of the week memory trick

0   s, z, soft c
1   t, d, th
2   n
3   m
4   r
5   l
6   j, sh, soft ch, dg, soft g
7   k, hard ch, hard g, ng, qu
8   f, v
9   b, p


figure out any day of the week between 1700 and 2199:


century:          1700,1800,1900,2000,2100
century code:      20542
phoenetic version: NZLRN
memory phrase:     NaZuL RaiN
eg: 2036 -> 4

year1: two digit year (without all of its sevens)
eg: 2036 -> 36 -> 36-(7*5)=1

year2: two digit year divided by 4 (ignore remainder and remove all sevens)
eg: 2036 -> 36/4=9 -> 9-(7*1)=2

month: J,F,M,A,M,J,J,A,S,O,N,D
month code:        366 240 251 361
phoenetic version: MJJ NRS NLT MJT
memory phrase:     MuCH NoRSe NuLleD MatCHeD
eg: June -> 0

day: day number (remove all possible sevens)
eg: 29 -> 29-(7*4)=1

total and remove all sevens

answer:      0,1,2,3,4,5,6
day of week: S,S,M,T,W,T,F

example: July 31 1882
    century + year1 + year2 + month + day
    0 + [82-(11*7)] + [(82/4)-(2*7)] + 2 + 31-(4*7)
    0 + 5 + 6 + 2 + 3
    16-(2*7)
    2
    Monday
