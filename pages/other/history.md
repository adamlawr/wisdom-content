categories: other

# history

The Past Billion Years

PROTEROZOIC EON (NEO-PROTEROZOIC)

Tonian Period (1,000 Million Years - 720 Million Years)

    Single-celled organisms are prolific in the Earth's oceans. Acritarchs, believed to be a kind of primitive plankton - were abundant. No complex life yet exists, and continents are barren. The continents are tectonically drifting southward and assembling into a supercontinent, which will be called Rodinia. The planet begins to cool as the far-southern continents begin to glaciate.

Cryogenian Period (720 - 635 Million Years)

    The supercontinent Rodinia completely assembles in the southern hemisphere, with its interior straddling the south-pole. Runaway icehouse conditions cause the planet to plunge into the coldest ice-age ever. All of the supercontinent is covered in glaciers, and even the north-pole, which was nothing but open ocean, begins to form sea ice. By the peak of the Cryogenian, the entire planet is covered in nearly a kilometer of ice - which scientists lovingly dub "snowball earth". Any would-be alien passerby's would see only a giant iceball in space. Despite these extreme conditions, live continues to evolve relatively oblivious - as all life is essentially hardy microbes. Complex life does evolve at this time, however. Primitive sponges about a quarter the size of a pinky-nail, and jellyfish. Rodinia begins to break apart through intense volcanic rifting, resulting in temperatures rising.

Ediacaran Period (635 - 541 Million Years)

    The Earth thaws out & Rodinia has broken up into smaller continents. Primitive complex life evolves in force across the oceans. Most of these organisms are "nature's experiments", in that nearly none of them will survive past the Ediacaran, they resemble nothing that followed after them. If you could swim around in the Ediacaran seas, you would likely believe you were on an alien planet, that's how weird these animals looked.

PHANEROZOIC EON

PALEOZOIC ERA

Cambrian Period (541 - 485 Million Years)

    The Cambrian Explosion takes place. It wasn't an actual explosion, calm down silly. It was an "explosion of life." Think of it this way, imagine a stadium and everyone in it represented a single species on the Earth. In the Ediacaran there were 250 people in the stadium...and then in the matter of about 5 minutes, there were suddenly 10,000 people! That's a huge jump in the different kinds of animals on the Earth in a short span of time, hence a figurative "explosion." Of course, this explosion took a couple million years, but geologically that's a blink. The first superpredator, Anomalocaris is on the hunt (it only got like 3 feet long, but at the time that was huge). Trilobites evolve and proliferate as the little iconic seabugs people see when they read about paleontology. Primitive chordates evolve, which will give rise to fish..and all vertebrates in an age to come. The continents are still barren.

Ordovician Period (485 - 444 Million Years)

    Life continues to evolve in the oceans at a brisk pace. Echinoderms like sea-urchins and sea-lilys evolve, so life is beginning to look a little more recognizable. Fish evolve, but are jawless - they just swim around sucking up muck. Mollusks, which evolved in the Cambrian, get big in the Ordovician, like really big (think as a big as a school bus). Arthropods continued to diversify, with sea-scorpions becoming another major predator. Continents were still barren, all life still lived in the ocean.

=========THE FIRST MAJOR EXTINCTION, THE ORDOVICIAN-SILURIAN EXTINCTION=========

    Was actually a 2-staged extinction pulse that killed off 27% of all families, 47% of all genera, and 65% of all species. It is considered the second worse mass extinction in Earth history, and is believed to be the result of sudden ice-ages. Though there are those who argue it could have been the result of a much more terrifying possibility - a gamma-ray burst.

Silurian Period (444 - 419 Million Years)

    The ice-ages recede, and most of the Earth is quite inundated with water. A majority of the continents are covered in shallow seas, and organisms love shallow seas...particularly shallow tropical seas. Nautiloids mollusks are still major players in the food chain, as are sea-scorpions (what's crazy is that these ancient sea-scorpions were distantly related to modern day scorpions! How neat is that?) Fish continue to evolve, and though they are still jawless, have developed bony plate-armor on their bodies. Primitive tiny plants begin to colonize the shores of the continents.

Devonian Period (419 - 359 Million Years)

    Fish evolve jaws, and shit just got real for every other creature on the planet. A primitive group of jawed fish, called Placoderms, become the major players in the Earth's oceans. Dunkleosteus is considered the first fish superpredator. They would have been pretty scary, no doubt about it. Nautiloid mollusks continue to evolve, though never again will they be the big-boys of the seas. The colonization of the continents is underway! At first simple fungus-biomes (minecraft lol) existed on the shores, but soon primitive fern-forests take over along the coastal regions. Some bony fish crawl out of the water to get on land, no doubt to get away from Dunkleosteus, and behold! The first amphibians! Bugs have long since moved on to land, with insects and arachnids crawling about for the first amphibians to munch on.

=========THE SECOND MAJOR EXTINCTION, THE LATE DEVONIAN EXTINCTION===========

    This event affected marine life almost exclusively - land animals seemed to have gone through it relatively fine. Massive anoxic events are believed to be the culprit behind the Late Devonian Extinction, in which oxygen levels plummeted to dangerously low levels - causing mass die offs. Lots of debate as to what caused the anoxic conditions, but no doubt low oxygen levels were the problem. Particularly hard hit were coral reefs, which would survive - but only just. Corals reefs would not experience a resurgence until the Mesozoic, over a hundred million years later. Placoderms, those scary-ass armor plated killing machines, die out completely. All of them. Never will the tree of life suffer such a complete die off of a clade until the death of the dinosaurs. Goodbye placoderms, we hardly knew ye. 19% of all families, 50% of all genera, and 70% of all species go extinct.

Carboniferous Period (359 - 299 Million Years)

    The placoderms are gone, but another fish is eager to fill their now vacant job position. Sharks have arrived, and the world will never be the same. Sharks had been around since the Devonian, but had relegated to minor roles in the ecosystem, with the placoderms out of the picture they now were able to quickly diversify and take over nearly all roles of fish. The Carboniferous was a golden-age for sharks, as sharks made up nearly 2/3 of all fish in the water. A type of freshwater shark, called xenacanths, even began to proliferate in the Carboniferous. They looked like a cross between a shark and an eel...yay. The continents are still inundated with tropical seas, so nearly all land above water is covered in tropical fern forests. Oxygen levels in the atmosphere begin to spike to dangerous levels (oxygen is a volatile gas!), with o2 levels reaching 33%. Intense forest-fires raged across the planet due to oxygen burn-offs from the high levels of o2. Fun fact, the higher the oxygen levels, the bigger bugs can get. So in the Carboniferous dragonflys got to be as big as eagles, spiders got as a big as house cats, cockroaches got as a big as dinner platers, and millipedes got as big as motorcycles. It was a golden age for bugs, too. Amphibians continued to diversift and evolve, with a type of crocodile-like amphibian called a labyrinthodont as a major land predator. But I saved the best for last! A new type of land vertebrate has evolved that has scales and sealed-eggs...reptiles! Reptiles are now on the evolutionary scene. The continents begin to merge into a supercontinent, this one will be called Pangea.

    permalinkembedsaveparentreportgive goldreply

[–]bigmac80 907 points 6 hours ago*x2

Permian Period (299 - 252 Million Years)

    All the continents have merged to form Pangea. The interior of the continent becomes dry and barren. These arid & colder conditions spell the end for the fern forests and the swamp animals that lived in them. A new type of land plant appears that can produce its own natural form of antifreeze, and is better able to retain moisture...these plants are conifers - the same group of plants that include pine trees. Conifer forests take over across the continent where it isn't totally inhospitable. Amphibians don't do so well in the Permian, surprise surprise. Reptiles, which evolved in the Carboniferous, are separated into two clades: Synapsids and Diapsids. People are often told that mammals are an offshoot of reptiles, but this isn't entirely true. The lineage that gave rise to mammals split from other reptiles almost immediately after reptiles evolved. Diapsids are the group of reptiles that would give rise to dinosaurs, birds, lizards, turtles (I like turtles), and crocodiles. Synapsids, though once diverse, would eventually be just mammals. Fun fact, the Dimetrodon, was a synapsid, not a diapsid...one of our distant relatives, how neat is that? Marine life struggles to survive in less and less marine habitats as more of the continents close up to form Pangea. Land life struggles to survive in an ever-drying out environment as more of the supercontinent becomes a desert.

=========THE THIRD MAJOR EXTINCTION, THE PERMIAN-TRIASSIC EXTINCTION===========

    Also called "The Great Dying." This one is the grandaddy of all extinctions. Life was already in bad shape by the end of the Permian, what with all the deserts and crummy marine life conditions, so what follows is just more than the world could bear. Pangea begins to rift apart, and in doing so massive volcanics begin to occur (particularly in what is now modern-day Siberia). This is called the Siberian Traps, and is essentially a mountain range the size of Texas of non-stop erupting volcanoes. Lots of ash, lots of greenhouse gases, lots of toxins. Oceans become anoxic, life begins to die. Oxygen levels even in the atmosphere are low, land life dies. It is the greatest of all extinctions - with 57% of all families, 83% of all genera, and 96% of all species die out. Some notably casualties include trilobites, therapsids, xenacanths, and labyrinthodonts.

MESOZOIC ERA

Triassic Period (252 - 200 Million Years)

    Ok, before I go any further I got to tell you about Lystrosaurus, because I find it so goddamn funny. The Permian Extinction was bad right, nearly everything on the planet got handed the pink slip. But not the Lystrosaurus, oh no. Not only did it survive, it took over nearly every plant-eater niche on land. Think of if this way, imagine nearly all plant-eaters on Earth died out, except for pigs. Now imagine pigs took over everywhere as a result. Replace all deer with pigs. Replace all horses with pigs. Elephants get replaced with pigs. Hippos too, for that matter. Elk are gone, just pigs. And you can forget about oxen & sheep. Make those pigs too. Pigs...PIGS. EVERYWHERE PIGS. Well that's what happened with the Lystrosaurus as the start of the Triassic, that thing was all over the place since pretty much all the plant eaters died out.

    Ok, back on topic - the Triassic. Dinosaurs evolve! They start out tiny, but they'll get bigger in time, boy will they get bigger. Some synapsids continue to evolve, of which a group called mammaliaforms appear....sound kinda familiar? Eh? Eh? Sea reptiles evolve, with Ichthyosaurus being an iconic one from that time. Turtles are doing their thing. Pterdons evolve and are the first true-flying vertebrates. Mosquitos evolve.....hooray.

=========THE FOURTH MAJOR EXTINCTION, THE TRIASSIC-JURASSIC EXTINCTION==========

    Believed to be caused by global cool and a spout of intense volcanics as North America began to rift away from Africa (fun fact, Florida is a remnant piece of Africa that North America got to keep after rifting away). The last of the large amphibians, synapsids, and non-dinosaur reptiles (like crocodiles) were all badly hit - leaving dinosaurs little competition. 23% of all families, 48% of all genera, and 75% of all species went extinct.

Jurassic Period (200 - 145 Million Years)

    Another golden age of sharks kicks off. A new kind of modern-form sharks evolve, most of all the sharks we know today come from this lineage. Rays and sawfish also evolve. Nautiloids (remember them?) are still around, with ammonites also enjoying a burst of diversity. Dinosaurs get big, really big. Allosaurus is an alpha predator, and brachiosaur herds are stripping entire pine forests of their needles. A new kind of land plant, deciduous plants, begins to diversify as they grow faster and can recover from dinosaur grazing faster than conifers. Pterodons continue to dominate the skies, but a new winged vertebrate is muscling into their turf - birds. Ichthyosaurs are still around, as are plesiosars and pliosaurs. The biggest fish ever to evolve, Leedsichthys appeared in the Jurassic - it got as big as blue whale. Here's a bonus pic of a pliosaur called Liopleurodon swimming near one.

Cretaceous Period (145 - 66 Million Years)

    The Earth is getting hot, like steamy hot. Median ocean temperatures are something like 90 degrees. Dinosaurs continue to evolve and diversify, though the giant long-neck dinosaurs disappear from all the continents except South America, where they not only survive - but get even bigger. Titanosaurs should be a dead ringer that these things get huge. Gigantosaurus, a T-Rex like predator - but bigger, had to hunt in packs to bring down their prey, Titanosuars got that big. Speaking of T-Rex, it's out and about, as are raptors. Iguanadons give rise to duck-billed dinosaurs, and triceratops is grazing in the meadows. Pteradons are all but gone, they just can't compete with birds. Asteroid impact or not, pteradactyls would not have survived much longer, they were a dead clade walking. Ichthyosaurs and Pliosaurs are gone, but Mosasaurs are here to take their place. Mosasaurs didn't get as big as Pliosaurs (despite what Jurassic World would have you believe), but hunted in packs like a dino-age orca. They were also related to snakes, so think about that. Cross a python with an orca and you got a mosasaur. Snakes evolve too, obviously. Ants evolve from wasps, yay. JUST YAY. Primitive mammals evolve!!

=======THE FIFTH MAJOR EXTINCTION, THE CRETACEOUS-PALEOGENE EXTINCTION========

    A lot went wrong for the K-Pg extinction to happen. Most people pin it on the asteroid impact, but it wasn't alone in this. Not saying a 10 mile-wide chunk of rock slamming into Mexico at 16,000 miles an hour did the Earth any good, but it wasn't the only problem that lead to the death of the dinosaurs. The Earth was in bad shape prior to the Chicxulub impact. First off, intense volcanics (notice this keeps popping up when an extinction happens?) was occurring in what is now western-India. This was the Deccan Traps event, and though not as bad as the Siberian Traps in the Permian - it was still bad news. The planet was in the throws of a runaway green house, oxygen levels in the oceans were somewhat lower, and then you throw a giant rock at our planet and you're bound to get some serious die-offs. And so went the dinosaurs. And ammonites. And all marine reptiles except turtles (I like turtles). 17% of all families, 50% of all genera, and 75% of all species became extinct.

THE CENOZOIC ERA

Because the geologic record is more complete for the the Cenozoic, we are able to look at and discuss the Age of the Mammals in more detail than the preceding spans of time. As a result, I will cover the Epochs of the Cenozoic, not just the Periods.

The Paleogene Period

The Paleocene Epoch (66 - 56 Million Years)

    The dinosaurs are gone, but their tropic world is not. Not only is the planet still steamy hot, it's actually getting even hotter. People are told that mammals inherited the Earth after dinosaurs died out, but like most inheritances - it was hotly contested. Not only were mammals rapidly diversifying in a world without dinos, but so were birds. And while mammals succeeded in taking over a lot of ecological niches, the birds got the upperhandwing on reaching the top of the food chain. Terrorbirds evolved in the Paleogene, and might as well have been dinosaurs 2.0. Titanoboa, the largest snake ever to live, was in South America swallowing 20-foot crocodiles whole. The oceans remain relatively empty, sharks for some reason never diversified in a 3rd golden age.

The Eocene Epoch (56 - 34 Million Years)

    The planet finally peaks in temperature at the start of the Eocene in what is called the Paleocene–Eocene Thermal Maximum. After that, the Earth finally begins to cool off. Why? Well, India is just beginning to plow into Asia - so the Himalayas are starting to rise. Mountains disrupt weather, and the Himalayas will certainly be doing that. Grass evolves! As the planet cools, the tropical forests begin to give way to savannas of this new grass. Mammals actually do better in colder environments, and the changing climate of the Eocene is what finally tips the scales in our favor over birds (suck it birds). Primitive whales take to the seas. The first horses begin grazing on all this awesome grass. The first predators were ungulates, as carnivora hadn't evolved yet. Andrewsarchus was one such alpha predator. Its claws were actually specialized hooves, how neat is that?

    permalinkembedsaveparentreportgive goldreply

[–]bigmac80 669 points 5 hours ago

The Oligocene Epoch (34 - 23 Million Years)

    The Earth continues to cool. Australia begins to rift from Antarctica, it's a bitter divorce - and Australia pretty much gets everything. The problem was, Antarctica has pretty much sat around the south pole for the past 100 million years, it had stayed relatively warm by being connected to Australia. Australia was more northerly located, and diverted warmer currents south towards the far south. Once Australia rifted away, Antarctica began to slowly freeze solid, dooming nearly all life on the continent to an icy grave. The first carnivores (dogs, cats, wolves, bears, etc) evolve at this time, though they looked like weasels and nothing more. Terrorbirds are gone from nearly all continents except for Australia and South America. Marsupials, which I guess I should have mentioned in more detail, have long since been replaced with more modern mammals on all continents except for Australia and South America also.

The Neogene Period

The Miocene Epoch (23 - 5.3 Million Years)

    Antarctica freezes solid, the Himalayas continue to rise, and North & South America form a land-bridge connecting between the two. The formation of the Central American land-bridge allowed for what is called the Great American Interchange, as tons of animals migrated between the two continents. Terror birds moved into North America and set up shop in the Florida region. Mammals like the big cats (sabretooth tigers!) migrated into South America. Modern whales evolve (their blow holes used to be much closer to their faces). Of which the predatory sperm whale called Livyatan melvillei hunted other whales. Hominidae, great apes, evolve...we are on our way to making humans!

The Pliocene Epoch (5.3 - 2.58 Million Years)

    The largest shark ever to evolve, Megalodon is terrorizing the oceans. The Earth is getting seriously cold, in fact this is the coldest the Earth has been all the way back to the Cryogenian. We have ice at both poles. Now that may seem like business as usual, but in the context of the past billion years that's a big deal. Mammoths evolved to take advantage of the spreading tundras. Continents pretty much resemble what we know today. Australopithecines evolve, these are the forebearers of humanity.

The Quaternary Period

The Pleistocene Epoch (2.58 Million Years - 11,700 Years)

    Megalodon goes extinct just as ancestral humans appear, and you thought God didn't do you any favors. Ice-ages continue to wrack the planet in pulses. Mankind was divided into some notable lineages, with Humans, Neanderthals, and Denisovans. Of which, only we humans remain. Neanderthals all went extinct in the second-to-last ice age 40,000 years ago; and the Denisovans went extinct not too long after that, but we're still hashing out exactly when. Mammoths were on the verge of extinction. The woolly rhino had gone extinct, as did the cave lion and cave bear. Some believe that over hunting by humans played a major role in these extinctions, though some argue the changing climate (which at the time we humans could not have influenced) played a more significant role in their dying off.

The Holocene Epoch (11,700 Years - Present Day)

    The rise of humankind. Grab a history book and dive in. I could write about it, because I think early civilizations were cool as shit, but I'm tired as hell now.
