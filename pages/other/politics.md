# categories: other

## pierre poilievre
- [marching](https://www.nationalobserver.com/2022/08/23/opinion/pierre-poilievre-dangerous-dance-diagolon-extremist)
- [convoy party](https://www.nationalobserver.com/2022/07/07/opinion/cpc-now-convoy-party-canada)

## john rustad
- how are carbon emissions a problem for carbon based beings? [youtube](https://www.youtube.com/watch?v=nTSdcx85Sk0&t=1617s)
- [climate consensus](https://news.cornell.edu/stories/2021/10/more-999-studies-agree-humans-caused-climate-change)


# us/canada trade
- [us](https://www.census.gov/foreign-trade/balance/c1220.html)

- [canada's trade partners](https://en.wikipedia.org/wiki/List_of_the_largest_trading_partners_of_Canada)

$ 140 billion imports from the US, $ 75 billion from the next biggest partner. most of the stuff on our shelves wrapped in plastic is coming from the US.