categories: other

# wages

- [wealth concentration](https://www.commondreams.org/news/2019/12/09/staggering-new-data-shows-income-top-1-has-grown-100-times-faster-bottom-50-1970)
> The tiny number of people raking in the overwhelming majority of the last 40 years of economic growth are distorting the economy and the political system like a black hole, everything falling toward their interests at high speed

## history

- [how the rich got rich](https://getpocket.com/explore/item/this-cartoon-explains-how-the-rich-got-rich-and-the-poor-got-poor)

## employment

- [unemployment rate goes down](https://globalnews.ca/news/3429004/unemployment-rate-canada/) but:
> A closer look at the data showed a loss of 50,500 positions in the more-desirable private-sector category, while the public sector added 35,200 jobs. There were also 31,200 fewer full-time jobs last month, while the number of part-time positions grew by 34,300

## corporate profits

- in the last 10 years, [corporate profits](https://tradingeconomics.com/canada/corporate-profits) have more than quadrupled (400%) from 20000 (CAD millions) to 85000 (CAD millions)
while [average hourly wages](https://tradingeconomics.com/canada/wages) have gone from $20 to $26 (25%) and [minimum wage](https://tradingeconomics.com/canada/minimum-wages) has gone from $9.50 to $15 (37%)
look at the historical charts and change the range to 10 years

## wage stagnation

- [global news](https://globalnews.ca/news/3531614/average-hourly-wage-canada-stagnant/)
- [stagnated](http://www.pewresearch.org/fact-tank/2018/08/07/for-most-us-workers-real-wages-have-barely-budged-for-decades/)

here's data from statscan that says real hourly wages have increased 14% from 1981 to 2011
[statscan](https://www150.statcan.gc.ca/n1/pub/11f0019m/2013347/part-partie1-eng.htm)
here's the [chart](https://www150.statcan.gc.ca/n1/pub/11f0019m/2013347/ct001-eng.htm)
here's data that indicates inflation has caused $100 in 1981 to be equivelant to $232 in 2011 (far more than a 14% 1ncrease)

## minimum wage

- [does not automatically create inflation](https://benjaminstudebaker.com/2015/07/23/misconceptions-raising-the-minimum-wage-does-not-automatically-lead-to-inflation/)
> ... relies on the assumption that firms pay their workers the maximum amount they can possibly pay while staying in business, such that any increase in wages necessitates raising prices. This could not be further from the truth–firms do not try to maximize wages, they try to maximize profits

## housing

- [historical pricing](https://www.cnbc.com/2017/06/23/how-much-housing-prices-have-risen-since-1940.html)
> Even adjusted for inflation, the median home price in 1940 would only have been $30,600 in 2000 dollars

> It's natural for prices to rise over time. But the issue here is that home values are outpacing inflation, making it nearly impossible for new and young buyers to enter the market

