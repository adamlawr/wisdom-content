Categories: other

# interview questions

## canditate questions
1. why does this role exist? new project? replacing someone?
2. how can i kick ass at this job?


## interview questions:

### rails

- what are the 7 controller actions you get when generating a scaffold?
	> index, show, new, create, edit, update, destroy

what generator would you use if you want all the routes generated but none of the controller actions?
	> rails g resource

### general

- do you know what the n+1 bug is?
	> lazy-loading of a collection can cause n+1 db queries when you ask for child objects in a loop
	> fix with eager-loading using includes:
	> Article.includes(:authors)
