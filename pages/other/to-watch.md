categories: other
views: 17
last viewed: 2020-06-01

# stuff to watch
## netflix
- the ballad of buster scruggs
- private life
- orange is the new black
- bojack horseman
- succession

# stuff to download
- it’s always sunny in philadelphia
- the duece (hbo)
- the americans (fx)
- high maintenance (hbo)
