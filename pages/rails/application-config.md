Categories: rails

# rails 5.2 credentials

## set an editor
- `EDITOR=atom --wait`
- restart the terminal
## edit credentials
- `rails credentials:edit`
this will:
- create `/config/master.key` if necessary and add it to `/.gitignore`
- create `/config/credentials.yml.enc`

** don't commit master.key to source control **
